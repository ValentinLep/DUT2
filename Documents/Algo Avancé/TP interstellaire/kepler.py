#! /usr/bin/env python3

import pygame
import sys
import json
from planete import Planete
from vaisseau import Vaisseau

global FPSCLOCK
FPS = 120
WINDOWWIDTH = 1800
WINDOWHEIGHT = 1600
ARRIERE_PLAN = (42,17,51)
PI = 3.14159265358


class Quitte(Exception ):
    pass

def isQuitEvent(event):
    return (event.type == pygame.QUIT or 
            (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE))

def handleKey(event):
    print("appui sur la touche", event.key)

def handleClick(event):
    print("Clic à la position", event.pos)

def handleEvents():
    for event in pygame.event.get():
        # pour chaque évènement depuis le dernier appel de cette fonction
        if isQuitEvent(event):
            raise Quitte
        elif event.type == pygame.KEYDOWN:
            handleKey(event)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            print("clic")

def drawApp(s_temp, s_pers, ecran, univers):
    """
    Redessine l'écran.
    """
    s_temp.fill((0,0,0,0)) # on remplit avec du transparent
    for objet in univers:
        objet.dessine(s_pers, s_temp)
    ecran.fill(ARRIERE_PLAN)
    ecran.blit(s_pers, (0,0))
    ecran.blit(s_temp, (0,0))
    pygame.display.update()
    
def main():
    temps = 0
    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    pygame.display.set_caption('Space Oddity')
    info = pygame.display.Info()
    WINDOWWIDTH, WINDOWHEIGHT = info.current_w,info.current_h
    ecran = pygame.display.set_mode((0,0), pygame.RESIZABLE)
    s_pers = pygame.Surface((WINDOWWIDTH, WINDOWHEIGHT), pygame.SRCALPHA)
    s_temp = pygame.Surface((WINDOWWIDTH, WINDOWHEIGHT), pygame.SRCALPHA)

    etoile = Planete("etoile", 30, 0, (400, 400), 0, (255, 255, 255))
    planete = etoile.add_satellite("planete", 30, 200, 3.01, (22,45,123))
    planete2 = etoile.add_satellite("planete2", 30, 500, 1.51, (22, 245,123))
    lune1 = planete.add_satellite("lune1", 20, 70, -12, (223,45,76))
    lune2 = planete.add_satellite("lune2", 10, 100, 10, (223,45,76))
    lune3 = planete2.add_satellite("lune3", 10, 100, 10.01, (223,145,76), True)
    sonde = lune2.add_satellite("sonde", 3, 12, 50.01, (255, 255, 76), True)
    sonde2 = lune3.add_satellite("sonde2", 3, 12, 50.01, (255, 255, 76), True)

    def parcours_prefixe(planete):
        if len(planete.get_satellites()) == 0:
            return [planete]
        res = [planete]
        for sat in planete.get_satellites():
            res.extend(parcours_prefixe(sat))
        return res

    def parcours_postfixe(planete):
        if len(planete.get_satellites()) == 0:
            return [planete]
        res = []
        for sat in planete.get_satellites():
            res.extend(parcours_prefixe(sat))
        return res + [planete]

    def parcours_largeur(planete):
        if len(planete.get_satellites()) == 0:
            return [planete]
        res = [planete]
        liste = [planete]
        while len(liste):
            pla = liste.pop(0)
            for sat in pla.get_satellites():
                res.append(sat)
                liste.append(sat)
        return res

    print(parcours_prefixe(etoile))
    print(parcours_postfixe(etoile))
    print(parcours_largeur(etoile))


    Queurque = Vaisseau((0,0), parcours_prefixe(etoile), (77, 77, 255), False, "tardis.png") # pos, destinations, couleur, trace=False, sprite=None

    SkaiAuCoeur = Vaisseau((0,0), parcours_postfixe(etoile), (255, 255, 255), False, "tardis.png")

    Genly_Ai  = Vaisseau((0,0), parcours_largeur(etoile), (255, 0, 0), False, "tardis.png")


    univers = [etoile, Queurque, SkaiAuCoeur, Genly_Ai]
    
    ecran.fill(ARRIERE_PLAN)

    while True:  #boucle principale
        try:
            handleEvents()
            drawApp(s_temp, s_pers, ecran, univers)

            temps_ecoule = FPSCLOCK.tick(FPS)
            for objet in univers:
                objet.avance(temps_ecoule)

        except Quitte:
            break
            
    pygame.quit()
    sys.exit(0)

main()
