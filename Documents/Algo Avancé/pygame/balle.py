import pygame
import sys
import math
from random import randint


class Balle:
    def __init__(self, pos, vit, coul, taille):
        self.pos = pos
        self.vit = vit
        self.coul = coul
        self.taille = taille

    def randomBalle():
        return Balle([40, 40], [randint(-40, 40), randint(-40, 40)], (randint(0, 255), randint(0, 255), randint(0, 255)), randint(5, 60))

    def avance(self, t, maxX, maxY):
        x, y = self.pos
        vx, vy = self.vit
        self.pos[0] = (x + vx * t / 300)
        self.pos[1] = (y + vy * t / 300)
        
        if self.pos[0] > maxX or self.pos[0] < 0:
            self.vit[0] *= -1

        if self.pos[1] > maxY or self.pos[1] < 0:
            self.vit[1] *= -1

    def draw(self, s):
        pygame.draw.circle(s, self.coul, self.pos, self.taille)

    def contient(self, position):
        x, y = self.pos
        posX, posY = position

        distanceX = posX - x
        distanceY = posY - y

        distance = math.sqrt(distanceX * distanceX + distanceY * distanceY)

        return distance < self.taille