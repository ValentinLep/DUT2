#! /usr/bin/env python3

import pygame
import sys
from balle import *

global FPSCLOCK
FPS = 30
WINDOWWIDTH = 800
WINDOWHEIGHT = 600
ARRIERE_PLAN = (42,17,51)
global score
score = 0

class Quitte(Exception ):
    pass

def isQuitEvent(event):
    return (event.type == pygame.QUIT or 
            (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE))

def handleKey(event):
    print("appui sur la touche", event.key)

def handleClick(event, balles):
    for b in balles:
        if b.contient(event.pos):
            balles.append(Balle.randomBalle())

            global score
            score += 1

            print("TOUCHÉ, score = ", score)



def handleEvents(balles):
    for event in pygame.event.get():
        # pour chaque évènement depuis le dernier appel de cette fonction
        if isQuitEvent(event):
            raise Quitte
        elif event.type == pygame.KEYDOWN:
            handleKey(event)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            handleClick(event, balles)

def refresh(s):
    s.fill(ARRIERE_PLAN)

temps_total = 0
    
def drawApp(s, t, font, balles):
    """
    Redessine l'écran. 't' est le temps écoulé depuis l'image précédente.
    """
    global temps_total
    temps_total += t
    
    refresh(s)



    for b in balles:
        b.avance(t, WINDOWWIDTH, WINDOWHEIGHT)
        b.draw(s)

    message = "Score : " + str(score)
    message = font.render(message, 1, (255,255,255))
    s.blit(message, (0,0))

def main():
    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    pygame.display.set_caption('Carre blanc sur fond blanc')
    ecran = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    font = pygame.font.Font(pygame.font.match_font('comicsans'),30)

    balles = [Balle.randomBalle()]

    refresh(ecran)

    while True:  #boucle principale
        try:
            handleEvents(balles)
            pygame.display.update()
            temps_ecoule = FPSCLOCK.tick(FPS)
            drawApp(ecran, temps_ecoule, font, balles)
        except Quitte:
            break

            
    pygame.quit()
    sys.exit(0)

main()
