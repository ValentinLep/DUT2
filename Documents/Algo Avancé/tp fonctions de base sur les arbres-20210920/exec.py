#!/usr/bin/env python3

from arbre import *
from math import *

def question1():
    arbre = Arbre('A', [Arbre('B', [Arbre('C', [])])])
    return(arbre)

print(question1())

def question2():
    arbre = Arbre('A', [Arbre('B', [Arbre('C', [Arbre('D', [])])])])
    return(arbre)

print(question2())

def question3():
    arbre = Arbre('A', [])

    for nom in ['B', 'C']:
        arbre.add(Arbre(nom, []))
    
    for nom in ['D', 'E', 'F']:
        arbre.enfants()[0].add(Arbre(nom, []))

    for nom in ['G', 'H']:
        arbre.enfants()[1].add(Arbre(nom, []))
    
    return(arbre)

print(question3())

def nb_feuilles(arbre):
    if arbre.est_feuille():
        return 1
    else:
        cpt = 0
        for enf in arbre.enfants():
            cpt += nb_feuilles(enf)
        return cpt

print("===========\nLe nb de feuilles des 3 arbres : ")
print(nb_feuilles(question1()))
print(nb_feuilles(question2()))
print(nb_feuilles(question3()))

def nb_noeuds_internes(arbre):
    if arbre.est_feuille():
        return 0
    else:
        cpt = 1
        for enf in arbre.enfants():
            cpt += nb_noeuds_internes(enf)
        return cpt

print("===========\nLes noeuds des 3 arbres : ")
print(nb_noeuds_internes(question1()))
print(nb_noeuds_internes(question2()))
print(nb_noeuds_internes(question3()))

def hauteur(arbre):
    if arbre.est_feuille():
        return 0
    else:
        """
        maxi = 0
        for enf in arbre.enfants():
            if max < hauteur(enf):
                max = hauteur(enf)
        return maxi + 1
        """
        return max([hauteur(enf) for enf in arbre.enfants()]) + 1

print("===========\nLes hauteurs des 3 arbres : ")
print(hauteur(question1()))
print(hauteur(question2()))
print(hauteur(question3()))

def degre(arbre):
    if arbre.est_feuille():
        return 0
    else:
        enf_max = max([degre(enf) for enf in arbre.enfants()])
        enf_direct = len(arbre.enfants())
        return max([enf_direct, enf_max])

print("===========\nLes degrés des 3 arbres : ")
print(degre(question1()))
print(degre(question2()))
print(degre(question3()))


print("======================\nARITHMETIQUE\n======================")


def expression_correcte(arbre):
    if arbre.etiquette() in ["+", "*"]:
        return len(arbre.enfants()) >= 2 and all([expression_correcte(enf) for enf in arbre.enfants()])
    else:
        return arbre.est_feuille()

assert expression_correcte(Arbre("+", [Arbre("4", []), Arbre("9", [])]))
assert expression_correcte(Arbre("+", [Arbre("*", [Arbre("9", []), Arbre("7", [])]), Arbre("9", [])]))
assert not expression_correcte(Arbre("+", [Arbre("*", [Arbre("9", []), Arbre("7", [Arbre("7", [])])]), Arbre("9", [])]))
assert not expression_correcte(Arbre("+", [Arbre("*", [Arbre("9", []), Arbre("*", [])]), Arbre("9", [])]))
assert not expression_correcte(Arbre("+", [Arbre("*", [Arbre("9", []), Arbre("*", [Arbre("7", [])])]), Arbre("9", [])]))

print("IZI")

def valeur(arbre):
    if arbre.etiquette() == "+":
        return sum([valeur(enf) for enf in arbre.enfants()])
    if arbre.etiquette() == "*":
        produit = valeur(arbre.enfants()[0]) # on sait qu'il y a au moins 2 enfants
        for enf in arbre.enfants()[1:]:
            produit *= valeur(enf)
        return produit
    else:
        return int(arbre.etiquette())

assert( valeur(Arbre("+", [Arbre("4", []), Arbre("9", [])])) == 13)
assert( valeur(Arbre("+", [Arbre("*", [Arbre("9", []), Arbre("7", [])]), Arbre("9", [])])) == 72)

print("fonction valeur passe les tests")

print("=========================\nASSEMBLAGE\n=========================")

class Piece:
    def __init__(self, nom, cout, temps, enfants = []):
        self.__nom = nom
        self.__cout = cout
        self.__temps = temps
        self.__enfants = enfants
    
    def etiquette(self):
        return self.__nom + ", " + str(self.__cout) + ", " + str(self.__temps) + " heure"

    def get_nom(self):
        return self.__nom

    def get_cout(self):
        return self.__cout

    def get_temps(self):
        return self.__temps

    def get_enfants(self):
        return self.__enfants
    
    def add(self, nourisson):
        """
        ajoute un enfant à l'arbre
        """
        self.__enfants.append(nourisson)
    
    def __repr__(self):
        repr_enfants = ",".join(("%r" % e) for e in self.get_enfants())
        return "%r<%s>" % (self.etiquette(), repr_enfants)
    
    def est_feuille(self):
        return len(self.__enfants) == 0

guitare = Piece("guitare", 100, 1,
         [Piece("corps", 50, 1, [Piece("caisse", 100, 5), Piece("manche", 50, 3)]), 
         Piece("cordes", 10, 0)])

def prix(assemblage):
    if assemblage.est_feuille():
        return assemblage.get_cout()
    else:
        return sum([prix(enf) for enf in assemblage.get_enfants()]) + assemblage.get_cout()

assert prix(guitare) == 310

print("fonction prix(assemblage) passer le test")


def tempsAssemblage(assemblage):
    if assemblage.est_feuille():
        return assemblage.get_temps()
    else:
        return max([tempsAssemblage(enf) for enf in assemblage.get_enfants()]) + assemblage.get_temps()

assert tempsAssemblage(guitare) == 7

print("fonction tempsAssemblage a passé le test")