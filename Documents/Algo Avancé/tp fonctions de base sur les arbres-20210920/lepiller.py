from arbre import *

def abr1():
    # exo 1
    return Arbre("and", [Arbre("not", [Arbre(False, [])]), Arbre("or", [Arbre(True, []), Arbre(False, [])])])

arbre1 = abr1()
print(arbre1)

def abr2():
    # exo 1
    return Arbre("and", [Arbre(False, []), Arbre("or", [Arbre("not", [Arbre(False, [])]), Arbre(False, [])])])

arbre2 = abr2()
print(arbre2)

def nb_noeuds(abr):
    # exo 2
    if abr.est_feuille():
        return 1
    # retourne la somme du nombre de noeuds de chaque enfants + 1
    return sum([nb_noeuds(enf) for enf in abr.enfants()]) + 1

uneFeuille = Arbre("f", [])

assert nb_noeuds(arbre1) == 6
assert nb_noeuds(arbre2) == 6
assert nb_noeuds(uneFeuille) == 1
print("fonction nb_noeuds est passée")

def nb_noeuds_True(abr):
    # exo 3
    if abr.est_feuille():
        # "== True" car si on tombe sur un String, il serait True | if "not" -> True
        if abr.etiquette() == True: return 1
    # retourne la somme des nb_noeuds_True de chaque enfants dans abr.enfants() + 1 si je suis True
    if abr.etiquette() == True: jeSuisTrue = 1
    else: jeSuisTrue = 0
    return sum([nb_noeuds_True(enf) for enf in abr.enfants()]) + jeSuisTrue

uneFeuilleTrue = Arbre(True, [])

assert nb_noeuds_True(arbre1) == 1
assert nb_noeuds_True(arbre2) == 0
assert nb_noeuds_True(uneFeuille) == 0
assert nb_noeuds_True(uneFeuilleTrue) == 1
print("fonction nb_noeuds_True est passée")

def chemin(abr, ch):
    # exo 4
    for direction in ch:
        enf = abr.enfants()
        if direction > len(enf) or direction < 1: return None
        abr = enf[direction - 1]
    return abr

assert(chemin(arbre1, [2, 1]).etiquette() == True)
assert(chemin(arbre1, [1, 1]).etiquette() == False)
assert(chemin(arbre1, [2, 1, 1]) == None)
print("fonction chemin est passée")

def verif_arbre(arb):
    # exo 5
    nom = arb.etiquette()

    if nom not in ["and", "or", "not", True, False]: return False

    enf = arb.enfants()

    if nom in ["and", "or"] and len(enf) != 2: return False
    if nom == "not" and len(enf) != 1: return False
    if nom in [False, True] and not arb.est_feuille(): return False

    return all([verif_arbre(enf) == True for enf in arb.enfants()])

assert verif_arbre(arbre1)
assert verif_arbre(arbre2)
assert not verif_arbre(uneFeuille)
assert verif_arbre(uneFeuilleTrue)

assert not verif_arbre(Arbre("and", [Arbre(True, [])]))
assert not verif_arbre(Arbre("not", [Arbre(True, []), Arbre(False, [])]))
assert not verif_arbre(Arbre(True, [Arbre(True, [])]))
assert not verif_arbre(Arbre("test", [Arbre(True, [])]))
print("tests de verif_arbre(arb) sont passés")

def expression_bool(arb):
    # exo 6
    # je suppose que l'arbre est déjà vérifié sinon if not verif_arbre(arb): return False ou None
    nom = arb.etiquette()
    lesEnfants = arb.enfants()
    if nom == "and": return all(expression_bool(enf) for enf in lesEnfants) # ou expression_bool(lesEnfants[0]) and expression_bool(lesEnfants[1])
    if nom == "or": return any(expression_bool(enf) for enf in lesEnfants)
    if nom == "not": return not expression_bool(lesEnfants[0])
    return nom

assert expression_bool(arbre1)
assert not expression_bool(arbre2)
assert expression_bool(uneFeuilleTrue)
print("tests de expression_bool(arb) sont passés")

def arbreBool_to_string(arb):
    # exo 7
    res = ""
    nom = arb.etiquette()
    enf = arb.enfants()

    if nom == True: return "True"
    if nom == False: return "False"
    if nom in ["and", "or"]:
        return "(" + arbreBool_to_string(enf[0]) + ") " + nom + " (" + arbreBool_to_string(enf[1]) + ")"
    return "not(" + arbreBool_to_string(enf[0]) + ")"

assert(arbreBool_to_string(arbre1) == "(not(False)) and ((True) or (False))")
assert(arbreBool_to_string(arbre2) == "(False) and ((not(False)) or (False))")
assert(arbreBool_to_string(uneFeuilleTrue) == "True")
print("tests arbreBool_to_string(arb) sont passés")