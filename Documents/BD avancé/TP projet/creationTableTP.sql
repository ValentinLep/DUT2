DROP TABLE PRINCEPS;
DROP TABLE PRIXMEDIC;
DROP TABLE PRESCRIPTION;
DROP TABLE LOT;
DROP TABLE PHARMACIE;
DROP TABLE VILLE;
DROP TABLE PHARMACIEN;
DROP TABLE PATIENT;
DROP TABLE MEDECIN;
DROP TABLE MEDICAMENT;
DROP TABLE LABO;

CREATE TABLE LABO (
  nomL varchar(50),
  numTelLab varchar(10), -- dans fichier commentaire en 3.
  siteWLab varchar(50),
  PRIMARY KEY (nomL)
);

CREATE TABLE MEDICAMENT (
  principeActif varchar(50),
  dosage float,
  forme varchar(20),
  nomL varchar(50),
  nomMedic varchar(50),
  stabilite varchar(50),
  gout varchar(50),
  dissolution varchar(50),
  PRIMARY KEY (principeActif, dosage, forme, nomL),
  UNIQUE (nomMedic, dosage, forme, nomL)
);

CREATE TABLE PATIENT (
  SSNpat varchar(20), -- dans fichier commentaire en 3.
  prenomPat varchar(50), 
  nomPat varchar(50),
  codePPat int,
  dateNPat date,
  SSNmed varchar(20), -- dans fichier commentaire en 3.
  PRIMARY KEY (SSNpat)
);

CREATE TABLE MEDECIN (
  SSNmed varchar(20), -- dans fichier commentaire en 3.
  specialite varchar(50), 
  nbAnn int,
  telContact varchar(10), 
  SSNpatPerso varchar(20), -- dans fichier commentaire en 3.
  PRIMARY KEY (SSNmed)
);

CREATE TABLE PHARMACIEN (
  SSNphar varchar(20), -- dans fichier commentaire en 3.
  categorie varchar(50), 
  SSNpatPerso varchar(20), -- dans fichier commentaire en 3.
  PRIMARY KEY (SSNphar)
);

CREATE TABLE PRESCRIPTION (
  SSNpat varchar(20), -- dans fichier commentaire en 3.
  SSNmed varchar(20), -- dans fichier commentaire en 3.
  datePres date,
  principeActif varchar(50),
  dosage float,
  forme varchar(20),
  nomL varchar(50),
  numLot int, 
  nbPrise int, 
  nbJours int, 
  dateV date,
  numPH int,
  dateAchat date,
  PRIMARY KEY (SSNpat, SSNmed, datePres, principeActif, dosage, forme, nomL)
);

CREATE TABLE PHARMACIE (
  numPH int, 
  SSNphar varchar(20) NOT NULL, -- dans fichier commentaire en 3.
  addr varchar(70), 
  numTel varchar(10), -- dans fichier commentaire en 3.
  nomVille varchar(50),
  departement int,
  nomPH varchar(50),
  PRIMARY KEY (numPH),
  UNIQUE (nomVille, departement, nomPH)
);

CREATE TABLE VILLE (
  nomVille varchar(50), 
  departement int,
  nbHab int, 
  nbPH int,
  PRIMARY KEY (nomVille, departement)
);

CREATE TABLE PRIXMEDIC (
  numPH int, 
  principeActif varchar(50), 
  dosage float, 
  forme varchar(20),
  nomL varchar(50),
  prix float,
  PRIMARY KEY (numPH, principeActif, dosage, forme, nomL)
);

CREATE TABLE PRINCEPS (
  nomPrinceps varchar(30), 
  nomGenerique varchar(30), 
  nomL varchar(50),
  PRIMARY KEY (nomPrinceps)
);

CREATE TABLE LOT (
  numLot int, 
  principeActif varchar(50), 
  dosage float,
  forme varchar(20),
  nomL varchar(50),
  dateV date,
  PRIMARY KEY (numLot)
);

ALTER TABLE MEDICAMENT ADD FOREIGN KEY (nomL) REFERENCES LABO (nomL);
ALTER TABLE PATIENT ADD FOREIGN KEY (SSNmed) REFERENCES MEDECIN (SSNmed);
-- ALTER TABLE MEDECIN ADD FOREIGN KEY (SSNpatPerso) REFERENCES PATIENT (SSNpat); dans fichier commentaire en 2.
ALTER TABLE PHARMACIEN ADD FOREIGN KEY (SSNpatPerso) REFERENCES PATIENT (SSNpat);
ALTER TABLE PRESCRIPTION ADD FOREIGN KEY (SSNpat) REFERENCES PATIENT (SSNpat);
ALTER TABLE PRESCRIPTION ADD FOREIGN KEY (SSNmed) REFERENCES MEDECIN (SSNmed);
ALTER TABLE PRESCRIPTION ADD FOREIGN KEY (principeActif, dosage, forme, nomL) REFERENCES MEDICAMENT (principeActif, dosage, forme, nomL);
ALTER TABLE PRESCRIPTION ADD FOREIGN KEY (numPH) REFERENCES PHARMACIE (numPH);
ALTER TABLE PRESCRIPTION ADD FOREIGN KEY (numLot) REFERENCES LOT (numLot);
ALTER TABLE PHARMACIE ADD FOREIGN KEY (SSNphar) REFERENCES PHARMACIEN (SSNphar);
ALTER TABLE PHARMACIE ADD FOREIGN KEY (nomVille, departement) REFERENCES VILLE (nomVille, departement);
ALTER TABLE PRIXMEDIC ADD FOREIGN KEY (numPH) REFERENCES PHARMACIE (numPH);
ALTER TABLE PRIXMEDIC ADD FOREIGN KEY (principeActif, dosage, forme, nomL) REFERENCES MEDICAMENT (principeActif, dosage, forme, nomL);
ALTER TABLE PRINCEPS ADD FOREIGN KEY (nomL) REFERENCES LABO (nomL);
-- ALTER TABLE PRINCEPS ADD FOREIGN KEY (nomPrinceps) REFERENCES MEDICAMENT (nomMedic); -- à voir dans le fichier commentaire en 1.
-- ALTER TABLE PRINCEPS ADD FOREIGN KEY (nomGenerique) REFERENCES MEDICAMENT (nomMedic);
ALTER TABLE LOT ADD FOREIGN KEY (principeActif, dosage, forme, nomL) REFERENCES MEDICAMENT (principeActif, dosage, forme, nomL);
