INSERT INTO LABO(nomL, numTelLab, siteWLab) values 
    ("lab1", "0652917723", "val.fr"),
    ("lab2", "0669917723", "val2.fr");

INSERT INTO MEDICAMENT(principeActif, dosage, forme, nomL, nomMedic, stabilite, gout, dissolution) values
    ("paracetamol", 2.5, "gelule", "lab1", "doliprane", "jsp", "banane", "jspNonPlus");

INSERT INTO PATIENT(SSNpat, prenomPat, nomPat, codePPat, dateNPat, SSNmed) values
    ("669", "Vélimir", "Chakhnovski", 45160, TO_DATE("08/10/2021", "%d/%m/%Y"), "788"),
    ("667", "T", "R", 45160, TO_DATE("08/10/2021", "%d/%m/%Y"), "788"),
    ("668", "Théo", "Foltier", 45160, TO_DATE("08/10/2021", "%d/%m/%Y"), "788"),
    ("666", "Valentin", "Lepiller", 28260, TO_DATE("08/10/2021", "%d/%m/%Y"), "789");

INSERT INTO MEDECIN(SSNmed, specialite, nbAnn, telContact, SSNpatPerso) values
    ("789", "douleurs", 5, "0252917723", "669"),
    ("788", "champignons", 7, "0252917724", "666");

INSERT INTO PHARMACIEN(SSNphar, categorie, SSNpatPerso) values
    ("456", "gerant", "667");

INSERT INTO PRESCRIPTION(SSNpat, SSNmed, datePres, principeActif, dosage, forme, nomL, numLot, nbPrise, nbJours, dateV, numPH) values
    ("669", "666", TO_DATE("08/10/2021", "%d/%m/%Y"), "paracetamol", 2.5, "gelule", "lab1", 1, 2, 5, TO_DATE("15/10/2021", "%d/%m/%Y"), 1);

INSERT INTO PHARMACIE(numPH, SSNphar, addr, numTel, nomVille, departement, nomPH) values
    (1, "456", "255 Rue de l'Ardoux", "0652917723", "Olivet", "Loiret", "Phar'amp");

INSERT INTO VILLE(nomVille, departement, nbHab, nbPH) values
    ("Olivet", "Loiret", 2, 2);

INSERT INTO PRIXMEDIC(numPH, principeActif, dosage, forme, nomL, prix) values
    (1, "paracetamol", 2.5, "gelule", "lab1", 4);

INSERT INTO PRINCEPS(nomPrinceps, nomGenerique, nomL) values
    ("paracetamol", "doliprane", "lab1");

INSERT INTO LOT(numLot, principeActif, dosage, forme, nomL, dateV) values
    (1, "paracetamol", 2.5, "gelule", "lab1", TO_DATE("14/10/2021", "%d/%m/%Y"));