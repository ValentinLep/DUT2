insert into LABO values ('lab1', '0652917723', 'val.fr');
insert into LABO values ('lab2', '0669917723', 'val2.fr');

INSERT INTO MEDICAMENT values ('paracetamol', 2.5, 'gelule', 'lab1', 'doliprane', 'jsp', 'banane', 'jspNonPlus');

INSERT INTO MEDECIN values ('789', 'douleurs', 5, '0252917723', '669');
INSERT INTO MEDECIN values ('788', 'champignons', 7, '0252917724', '666');

INSERT INTO PATIENT values ('669', 'Vélimir', 'Chakhnovski', 45160, TO_DATE('27-07-2002', 'DD-MM-YYYY'), '788');
INSERT INTO PATIENT values ('667', 'T', 'R', 45160, TO_DATE('25-07-2002', 'DD-MM-YYYY'), '788');
INSERT INTO PATIENT values ('668', 'Théo', 'Foltier', 45160, TO_DATE('14-08-2002', 'DD-MM-YYYY'), '788');
INSERT INTO PATIENT values ('666', 'Valentin', 'Lepiller', 28260, TO_DATE('02 03 2002','MM DD YYYY'), '789');


INSERT INTO PHARMACIEN values ('456', 'gerant', '667');

INSERT INTO VILLE values ('Olivet', 45, 2, 2);

INSERT INTO PHARMACIE values (1, '456', '255 Rue de l Ardoux', '0652917723', 'Olivet', 45, 'Phar amp');

INSERT INTO PRIXMEDIC values (1, 'paracetamol', 2.5, 'gelule', 'lab1', 4);

INSERT INTO PRINCEPS values ('paracetamol', 'doliprane', 'lab1');

INSERT INTO LOT values (1, 'paracetamol', 2.5, 'gelule', 'lab1', TO_DATE('29-10-2021', 'DD-MM-YYYY'));

INSERT INTO PRESCRIPTION values ('669', '788', TO_DATE('08-10-2021', 'DD-MM-YYYY'), 'paracetamol', 2.5, 'gelule', 'lab1', 1, 2, 5, TO_DATE('15-10-2021', 'DD-MM-YYYY'), 1, TO_DATE('14-10-2021', 'DD-MM-YYYY'));