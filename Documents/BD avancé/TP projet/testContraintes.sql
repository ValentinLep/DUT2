-- tests de réinsersion des mêmes clés principales et clés uniques

insert into LABO values ('lab1', '0652917723', 'val.fr');
insert into LABO values ('lab1', '0652917777', 'val45.fr'); 
-- ne doit pas fonctionner (clé principale)
insert into LABO values ('lab2', '0669917723', 'val2.fr');

INSERT INTO MEDICAMENT values ('paracetamol', 2.5, 'gelule', 'lab1', 'doliprane', 'jsp', 'banane', 'jspNonPlus');
INSERT INTO MEDICAMENT values ('paracetamol', 2.5, 'gelule', 'lab1', 'test', 'test', 'test', 'test');
-- ne doit pas fonctionner (clé principale)
INSERT INTO MEDICAMENT values ('test', 2.5, 'gelule', 'lab1', 'doliprane', 'test', 'test', 'test');
-- ne doit pas fonctionner (clés uniques)

INSERT INTO MEDECIN values ('789', 'douleurs', 5, '0252917723', '669');
INSERT INTO MEDECIN values ('789', 'test', 0, '0277777777', '667');
-- ne doit pas fonctionner (clé principale)
INSERT INTO MEDECIN values ('788', 'champignons', 7, '0252917724', '666');

INSERT INTO PATIENT values ('669', 'Vélimir', 'Chakhnovski', 45160, TO_DATE('27-07-2002', 'DD-MM-YYYY'), '788');
INSERT INTO PATIENT values ('669', 'Vélimir2', 'Chakhnovski2', 45161, TO_DATE('28-07-2002', 'DD-MM-YYYY'), '789');
-- ne doit pas fonctionner (clé principale)
INSERT INTO PATIENT values ('667', 'T', 'R', 45160, TO_DATE('25-07-2002', 'DD-MM-YYYY'), '788');
INSERT INTO PATIENT values ('668', 'Théo', 'Foltier', 45160, TO_DATE('14-08-2002', 'DD-MM-YYYY'), '788');
INSERT INTO PATIENT values ('666', 'Valentin', 'Lepiller', 28260, TO_DATE('02 03 2002','MM DD YYYY'), '789');


INSERT INTO PHARMACIEN values ('456', 'gerant', '667');
INSERT INTO PHARMACIEN values ('456', 'caissier', '668');
-- ne doit pas fonctionner (clé principale)

INSERT INTO VILLE values ('Olivet', 45, 21639, 3);
INSERT INTO VILLE values ('Olivet', 45, 2, 2);
-- ne doit pas fonctionner (clé principale)

INSERT INTO PHARMACIE values (1, '456', '255 Rue de l Ardoux', '0652917723', 'Olivet', 45, 'Phar amp');
INSERT INTO PHARMACIEN values ('457', 'gerant', '668');
-- pour mieux tester la prochaine contrainte
INSERT INTO VILLE values ('Orleans', 45, 116238, 4);
-- pour mieux tester la prochaine contrainte
INSERT INTO PHARMACIE values (1, '457', '256 Rue de l Ardoux', '0652917777', 'Orleans', 45, 'Pharmacie1');
-- ne doit pas fonctionner (clé principale)
INSERT INTO PHARMACIE values (2, '457', '256 Rue de l Ardoux', '0652917777', 'Olivet', 45, 'Phar amp');
-- ne doit pas fonctionner (clé unique)

INSERT INTO PRIXMEDIC values (1, 'paracetamol', 2.5, 'gelule', 'lab1', 4);
INSERT INTO PRIXMEDIC values (1, 'paracetamol', 2.5, 'gelule', 'lab1', 5);
-- ne doit pas fonctionner (clé principale)

INSERT INTO PRINCEPS values ('paracetamol', 'doliprane', 'lab1');
INSERT INTO PRINCEPS values ('paracetamol', 'doliprane2', 'lab2');
-- ne doit pas fonctionner (clé principale)

INSERT INTO LOT values (1, 'paracetamol', 2.5, 'gelule', 'lab1', TO_DATE('29-10-2021', 'DD-MM-YYYY'));
INSERT INTO MEDICAMENT values ('paracetamol2', 3, 'poudre', 'lab2', 'doliprane2', 'test', 'fraise', 'test');
-- pour mieux tester la prochaine contrainte
INSERT INTO LOT values (1, 'paracetamol2', 3, 'poudre', 'lab2', TO_DATE('30-10-2021', 'DD-MM-YYYY'));
-- ne doit pas fonctionner (clé principale)

INSERT INTO PRESCRIPTION values ('669', '788', TO_DATE('08-10-2021', 'DD-MM-YYYY'), 'paracetamol', 2.5, 'gelule', 'lab1', 1, 2, 5, TO_DATE('15-10-2021', 'DD-MM-YYYY'), 1, TO_DATE('14-10-2021', 'DD-MM-YYYY'));
INSERT INTO PHARMACIE values (1, '456', '255 Rue de l Ardoux', '0652917723', 'Olivet', 45, 'Phar amp');
-- pour mieux tester la prochaine contrainte
INSERT INTO PRESCRIPTION values ('669', '788', TO_DATE('08-10-2021', 'DD-MM-YYYY'), 'paracetamol', 2.5, 'gelule', 'lab1', 2, 3, 6, TO_DATE('16-10-2021', 'DD-MM-YYYY'), 1), TO_DATE('14-10-2021', 'DD-MM-YYYY');
-- ne doit pas fonctionner (clé principale)

-- tests de clés étrangères

-- ALTER TABLE MEDICAMENT ADD FOREIGN KEY (nomL) REFERENCES LABO (nomL);
INSERT INTO MEDICAMENT values ('paracetamol3', 2.5, 'gelule', 'lab3', 'doliprane3', 'jsp', 'banane', 'jspNonPlus'); 
-- 'lab3' n'existe pas
-- ALTER TABLE PATIENT ADD FOREIGN KEY (SSNmed) REFERENCES MEDECIN (SSNmed);
INSERT INTO PATIENT values ('041670', 'Vélimir', 'Chakhnovski', 45160, TO_DATE('27-07-2002', 'DD-MM-YYYY'), '790'); 
-- le medecin 790 n'existe pas
-- ALTER TABLE PHARMACIEN ADD FOREIGN KEY (SSNpatPerso) REFERENCES PATIENT (SSNpat);
INSERT INTO PHARMACIEN values ('457', 'gerant', '051101302'); 
-- le patient '051101302' n'existe pas
-- ALTER TABLE PRESCRIPTION ADD FOREIGN KEY (SSNpat) REFERENCES PATIENT (SSNpat);
INSERT INTO PRESCRIPTION values ('777777', '788', TO_DATE('08-10-2021', 'DD-MM-YYYY'), 'paracetamol', 2.5, 'gelule', 'lab1', 1, 2, 5, TO_DATE('15-10-2021', 'DD-MM-YYYY'), 1, TO_DATE('14-10-2021', 'DD-MM-YYYY')); 
-- patient '777777' inexistant
-- ALTER TABLE PRESCRIPTION ADD FOREIGN KEY (SSNmed) REFERENCES MEDECIN (SSNmed);
INSERT INTO PRESCRIPTION values ('669', '7887777777', TO_DATE('08-10-2021', 'DD-MM-YYYY'), 'paracetamol', 2.5, 'gelule', 'lab1', 1, 2, 5, TO_DATE('15-10-2021', 'DD-MM-YYYY'), 1, TO_DATE('14-10-2021', 'DD-MM-YYYY'));
-- medecin '7887777777' inexistant
-- ALTER TABLE PRESCRIPTION ADD FOREIGN KEY (principeActif, dosage, forme, nomL) REFERENCES MEDICAMENT (principeActif, dosage, forme, nomL);
INSERT INTO PRESCRIPTION values ('669', '788', TO_DATE('08-10-2021', 'DD-MM-YYYY'), 'paracetamol4', 2.5, 'gelule', 'lab1', 1, 2, 5, TO_DATE('15-10-2021', 'DD-MM-YYYY'), 1, TO_DATE('14-10-2021', 'DD-MM-YYYY')); 
-- le medicament ('paracetamol4', 2.5, 'gelule', 'lab1') inexistant
-- ALTER TABLE PRESCRIPTION ADD FOREIGN KEY (numPH) REFERENCES PHARMACIE (numPH);
INSERT INTO PRESCRIPTION values ('669', '788', TO_DATE('14-10-2021', 'DD-MM-YYYY'), 'paracetamol', 2.5, 'gelule', 'lab1', 1, 2, 5, TO_DATE('15-10-2021', 'DD-MM-YYYY'), 6, TO_DATE('14-10-2021', 'DD-MM-YYYY')); 
-- pharmacie 6 inexistante
-- ALTER TABLE PRESCRIPTION ADD FOREIGN KEY (numLot) REFERENCES LOT (numLot);
INSERT INTO PRESCRIPTION values ('669', '788', TO_DATE('13-10-2021', 'DD-MM-YYYY'), 'paracetamol', 2.5, 'gelule', 'lab1', 678, 2, 5, TO_DATE('15-10-2021', 'DD-MM-YYYY'), 1), TO_DATE('14-10-2021', 'DD-MM-YYYY'); 
-- le lot 678 inexistant
-- ALTER TABLE PHARMACIE ADD FOREIGN KEY (SSNphar) REFERENCES PHARMACIEN (SSNphar);
INSERT INTO PHARMACIE values (47, '456897', '255 Rue de l Ardoux', '0652917723', 'Olivet', 45, 'Phar amp2'); 
-- le pharmacien 456897 est inexistant
-- ALTER TABLE PHARMACIE ADD FOREIGN KEY (nomVille, departement) REFERENCES VILLE (nomVille, departement);
INSERT INTO PHARMACIE values (48, '456', '255 Rue de l Ardoux', '0652917723', 'Olivette', 45, 'Phar amp3'); 
-- la ville 'Olivette', 45 n'existe pas
-- ALTER TABLE PRIXMEDIC ADD FOREIGN KEY (numPH) REFERENCES PHARMACIE (numPH);
INSERT INTO PRIXMEDIC values (18474, 'paracetamol', 2.5, 'gelule', 'lab1', 4); 
-- la pharmacie 18474 n'existe pas
-- ALTER TABLE PRIXMEDIC ADD FOREIGN KEY (principeActif, dosage, forme, nomL) REFERENCES MEDICAMENT (principeActif, dosage, forme, nomL);
INSERT INTO PRIXMEDIC values (1, 'paracetamol7', 2.5, 'gelule', 'lab1', 4); 
-- le médicament ('paracetamol7', 2.5, 'gelule', 'lab1') n'existe pas
-- ALTER TABLE PRINCEPS ADD FOREIGN KEY (nomL) REFERENCES LABO (nomL);
INSERT INTO PRINCEPS values ('paracetamol', 'doliprane', 'lab51'); 
-- le labo 'lab51' n'existe pas
-- ALTER TABLE LOT ADD FOREIGN KEY (principeActif, dosage, forme, nomL) REFERENCES MEDICAMENT (principeActif, dosage, forme, nomL);
INSERT INTO LOT values (244, 'paracetamol8', 2.5, 'gelule', 'lab1', TO_DATE('29-10-2021', 'DD-MM-YYYY')); 
-- le médicament ('paracetamol8', 2.5, 'gelule', 'lab1') n'existe pas

-- quelques petites contraintes autres

insert into LABO values (74, '0652917723', 'val.fr');
-- commentaire en 4.
INSERT INTO LOT values (1, 'paracetamol', 2.5, 'gelule', 'lab1', TO_DATE('29-10-2021', 'DD-MM-YYYY'));
-- test de mauvais type
insert into LABO values ('lab85', '06529177231', 'val.fr'); 
-- test de trop de caracteres dans un varchar (ici 11 caracteres dans le numéro de telephone)
