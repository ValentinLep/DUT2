select K1.VilleK as Ville1, K2.VILLEK as Ville2, K1.NomK from KIOSQUES K1, Kiosques K2 where K1.NomK = K2.NomK and K1.VilleK < K2.VilleK;


select NOmK, count(Titre) as NbTitre from VEND where VilleK = 'Orleans' group by NomK having count(Titre) > 100;


select Categorie, avg(PrixUnit) as prixMoyen
from JOURNAUX
group by Categorie;

-- 4
select Categorie, avg(PrixUnit) as prixMoyen
from JOURNAUX
group by Categorie
having avg(PrixUnit) >= ALL(select avg(PrixUnit) 
                            from JOURNAUX 
                            group by Categorie);

select Categorie, avg(PrixUnit) as prixMoyen
from JOURNAUX
group by Categorie
having avg(PrixUnit) = (select max(avg(PrixUnit))
                            from JOURNAUX 
                            group by Categorie);

-- 5
select NomK, VILLEK, Categorie, count(*) as nombreTitre
from JOURNAUX natural join VEND
group by NomK, VILLEK, Categorie;

-- 6
select distinct VILLEK
from VEND
group by VilleK, NOmK
having count(*) > ( select max(count(*))
                    from VEND
                    where VILLEK = 'Orsay'
                    group by NOmK, VILLEK);

-- 7
select Nom, Age, Sexe
from CLIENTS
where idCl not in (select distinct idCl
from CLIENTS natural join AIME natural join JOURNAUX
where TYPE != 'tabloid');

-- CORRECTION :
select nom from CLIENTS
where not exists ((select titre from JOURNAUX where type = 'tabloid') 
                    MINUS
                        (select titre from AIME where AIME.idCl = CLIENTS.idCl))