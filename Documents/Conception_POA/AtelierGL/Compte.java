
import java.util.*;

public class Compte {

	//
	// Fields
	//

  private Float solde;
  private String numero;
  private int decouvertAutorise;	public Compte () { };
  
	//
	// Methods
	//


	//
	// Accessor methods
	//

	/**
	 * Set the value of solde
	 * @param newVar the new value of solde
	 */
  public void setSolde (Float newVar) {
  	solde = newVar;
  }

	/**
	 * Get the value of solde
	 * @return the value of solde
	 */
  public Float getSolde () {
  	return solde;
  }

	/**
	 * Set the value of numero
	 * @param newVar the new value of numero
	 */
  public void setNumero (String newVar) {
  	numero = newVar;
  }

	/**
	 * Get the value of numero
	 * @return the value of numero
	 */
  public String getNumero () {
  	return numero;
  }

	/**
	 * Set the value of decouvertAutorise
	 * @param newVar the new value of decouvertAutorise
	 */
  public void setDecouvertAutorise (int newVar) {
  	decouvertAutorise = newVar;
  }

	/**
	 * Get the value of decouvertAutorise
	 * @return the value of decouvertAutorise
	 */
  public int getDecouvertAutorise () {
  	return decouvertAutorise;
  }

	//
	// Other methods
	//

	/**
	 * Constructeur
	 * @param        numero
	 */
  public Compte(String numero)
  {
		this.decouvertAutorise = 0;
		this.numero = numero;
		this.solde = 0;
	}


	/**
	 * @return       Boolean
	 * @param        montant
	 */
  public Boolean debiter(Float montant)
  {
		if (this.solde - montant > decouvertAutorise) {
			this.solde -= montant;
			return true;
		}
		return false;
	}


	/**
	 * @param        montant
	 */
  public void crediter(Float montant)
  {
		this.solde += montant;
	}


	/**
	 * @return       String
	 */
  public String toString()
  {
		return "numero de compte : " + this.numero + "\ndecouvert Autorise : " + this.decouvertAutorise + "\nsolde : " + this.solde;
	}


}
