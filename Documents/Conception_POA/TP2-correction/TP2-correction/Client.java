
import java.util.*;

public class Client {

  //
  // Fields
  //

  private String nom;

  private ArrayList<Compte> listeComptes = new ArrayList<Compte>();
  
  public void setNom (String newVar) {
    nom = newVar;
  }

  public String getNom () {
    return nom;
  }

  public Client(String nom)
  {
    this.nom = nom;
  }

  public Boolean ajouterCompte(Compte compte){
    String numero = compte.getNumero();
    for(Compte c : this.listeComptes){
      if(numero.equals(c.getNumero())){
        return false;
      }
    }
    this.listeComptes.add(compte);
    return true;
  }

  public Compte chercheCompte(String numero){
    for(Compte compte : this.listeComptes){
      if(numero.equals(compte.getNumero())){
        return compte;
      }
    }
    return null;
  }
}
