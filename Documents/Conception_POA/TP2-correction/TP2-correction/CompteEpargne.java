public class CompteEpargne extends Compte{

    private static double tauxInteret = 0.005;

  public static void setTauxInteret (double taux) {
    CompteEpargne.tauxInteret = taux;
  }

  public CompteEpargne(String numero){
      super(numero);
  }

  public boolean debiter(double montant)
  {
    double newSolde = this.solde - montant;
    if(newSolde >= 0){
      this.solde = newSolde;
      this.historique.add(0,new Operation(-montant));//on ajoute l'opération au début
      return true;
    }
    else{
      return false;
    }
  }

  @Override
  public String toString(){
    return super.toString()+"\n  Taux Intérêt :"+CompteEpargne.tauxInteret;
  }


}
