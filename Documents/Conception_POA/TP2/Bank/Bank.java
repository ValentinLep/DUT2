import java.util.Scanner;

public class Bank {
 public static void main(String[] args)
  {
    Client dupont = new Client("Dupont");
    //Compte compte = new Compte("CCP01234");
    //dupont.ajouterCompte(compte);
    //System.out.println(compte);
    String rep="",num;
    double montant;
    Compte compte;
    Scanner sc = new Scanner(System.in);

    while(!rep.equals("1")){
      // AFFICHAGE DU MENU
      System.out.println("GESTION DES COMPTES DE "+dupont.getNom());
      System.err.println("---------------------------------------");
      System.out.println("1. Quitter l'application");
      System.out.println("2. Consulter un compte");
      System.out.println("3. Débiter un compte");
      System.out.println("4. Créditer un compte");
      System.out.println("5. Créer un nouveau compte");
      System.out.println("7. Abonner un terminal à notifier")

      // INSTRUCTION UTILISATEUR
      rep = sc.nextLine();
      switch(rep){
        case "1": 
          System.out.println("Au revoir.");
          break;
        case "2":
          System.out.println("Numéro du compte à consulter :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            System.out.println(compte);
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;
        case "3":
          System.out.println("Numéro du compte à Débiter :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            System.out.println("Montant à Débiter :");
            montant = Double.parseDouble(sc.nextLine());
            if(compte.debiter(montant)){
              System.out.println("Le compte a été débité.");
            }
            else{
              System.out.println("Opération impossible.");
            }
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;
        case "4":
          System.out.println("Numéro du compte à créditer :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            System.out.println("Montant à créditer :");
            montant = Double.parseDouble(sc.nextLine());
            if(compte.crediter(montant)){
              System.out.println("Le compte a été crédité.");
            }
            else{
              System.out.println("Opération impossible.");
            }
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;
        case "5":
          System.out.println("Numéro du compte à créer :");
          num = sc.nextLine();
          System.out.println("compte courant (si oui : tapez 'O' si compte epargne tapez 'N') :");
          if (sc.nextLine().equals("O")) {
            if (dupont.ajouterCompte(new CompteCourant(num)))
            System.out.println("Le compte "+num+" a été ajouté.");
            else {System.out.println("Le compte "+num+" existe déjà en votre possession.");}
          } else {
            if (dupont.ajouterCompte(new CompteEpargne(num)))
            System.out.println("Le compte "+num+" a été ajouté.");
            else {System.out.println("Le compte "+num+" existe déjà en votre possession.");}
          }
          break;
        case "7":
          System.out.println("Numéro du compte :");
          num = sc.nextLine();
          compte = dupont.chercheCompte(num);
          if(compte != null){
            System.out.println("Montant à créditer :");
            montant = Double.parseDouble(sc.nextLine());
            if(compte.crediter(montant)){
              System.out.println("Le compte a été crédité.");
            }
            else{
              System.out.println("Opération impossible.");
            }
          }
          else{
            System.out.println("Ce numéro de compte n'existe pas!");
          }
          break;

      }
    }

    sc.close();
  }
}
