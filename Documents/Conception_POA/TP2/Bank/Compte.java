
public abstract class Compte extends Observable {

  //
  // Fields
  //

  private double solde;
  private String numero;
  private double decouvertAutorise;
  private List<Operation> historique;
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of solde
   * @param newVar the new value of solde
   */
  public void setSolde (double newVar) {
    solde = newVar;
  }

  /**
   * Get the value of solde
   * @return the value of solde
   */
  public double getSolde () {
    return solde;
  }

  /**
   * Set the value of numero
   * @param newVar the new value of numero
   */
  public void setNumero (String newVar) {
    numero = newVar;
  }

  /**
   * Get the value of numero
   * @return the value of numero
   */
  public String getNumero () {
    return numero;
  }

  //
  // Other methods
  //

  /**
   * @param        numero
   */
  public Compte(String numero)
  {
    super();
    this.numero = numero;
    this.solde = 0;
    this.historique = new ArrayList<>();
  }


  /**
   * @return       boolean
   * @param        montant montant à créditer (valeur positive)
   */
  public boolean crediter(double montant)
  {
    this.solde += montant;
    this.historique.add(0, new Operation(montant));
    this.notifierObservateurs("Votre compte vient d'être débiter de " + montant + ". Il est maintenant à " + this.solde + "€.");
    return true;
  }

  @Override
  public String toString(){
    return "Compte n°"+this.numero+"\n  Solde : "+this.solde+"\n  Decouvert autorisé :"+this.decouvertAutorise;
  }

}
