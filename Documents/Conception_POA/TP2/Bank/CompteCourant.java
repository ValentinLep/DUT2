
public class CompteCourant extends Compte {

	private double decouvertAutorise;	
	public CompteCourant (String numero) {
		super(numero);
		this.decouvertAutorise = 0;
	};
	/**
	 * Set the value of decouvertAutorise
	 * @param newVar the new value of decouvertAutorise
	 */
	public void setDecouvertAutorise (double newVar) {
		decouvertAutorise = newVar;
	}

	/**
	 * Get the value of decouvertAutorise
	 * @return the value of decouvertAutorise
	 */
	public double getDecouvertAutorise () {
		return decouvertAutorise;
	}

	/**
	* @return       boolean
	* @param        montant
	*/
	public boolean debiter(double montant)
	{
		double newSolde = this.solde - montant;
		if(newSolde >= -this.decouvertAutorise){
			this.solde = newSolde;
			this.historique.add(0, new Operation(montant));
			this.notifierObservateurs("Votre compte vient d'être débiter de " + montant + ". Il est maintenant à " + this.solde + "€.");
			return true;
		}
		else{
			this.notifierObservateurs("Vous avez essayer de débiter " + montant + " sur votre compte mais votre découvert autorisé ne le permet pas.\nPour rappel, votre découvert autorisé est de " + this.decouvertAutorise + "€.");
			return false;
		}
	}
}
