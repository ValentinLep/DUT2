
public class CompteEpargne extends Compte {

	private double tauxInteret = 0.5;	
	public CompteEpargne (String numero) {
		super(numero);
	};
	/**
	 * Set the value of tauxInteret
	 * @param newVar the new value of tauxInteret
	 */
	public void setTauxInteret (double newVar) {
		tauxInteret = newVar;
	}

	/**
	 * Get the value of tauxInteret
	 * @return the value of tauxInteret
	 */
	public double getTauxInteret () {
		return tauxInteret;
	}

	public boolean debiter(double montant)
	{
		double newSolde = this.solde - montant;
		if(newSolde >= 0){
			this.solde = newSolde;
			this.historique.add(0,new Operation(-montant));//on ajoute l'opération au début
			this.notifierObservateurs("Votre compte vient d'être débiter de " + montant + ". Il est maintenant à " + this.solde + "€.");
			return true;
		}
		else{
			this.notifierObservateurs("Vous avez essayer de débiter " + montant + " sur votre compte mais vous avez pas assez d'argent dessus...");
			return false;
		}
	}
}
