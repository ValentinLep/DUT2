
public class Observable {

	private List<IObservateur> observateurs;
	public Observable () {
		this.observateurs = new ArrayList<>();
	};


	//
	// Other methods
	//

	/**
	 */
	public void ajouterObservateur(IObservateur ob)
	{
		this.observateurs.add(ob);
	}


	/**
	 */
	public void supprimerObservateur(IObservateur ob)
	{
		this.observateurs.remove(ob);
	}


	/**
	 */
	public void notifierObservateurs(String message)
	{
		for (IObservateur ob : this.observateurs) {
			ob.notification(message);
		}
	}


}
