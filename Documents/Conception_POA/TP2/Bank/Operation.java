
import java.util.*;

public class Operation {

	//
	// Fields
	//

	private Date date;
	private int montant;	
    
    public Operation (int montant) {
        this.date = null;
        this.montant = montant;
    };
	
	//
	// Methods
	//


	//
	// Accessor methods
	//

	/**
	 * Set the value of date
	 * @param newVar the new value of date
	 */
	public void setDate (Date newVar) {
		date = newVar;
	}

	/**
	 * Get the value of date
	 * @return the value of date
	 */
	public Date getDate () {
		return date;
	}

	/**
	 * Set the value of montant
	 * @param newVar the new value of montant
	 */
	public void setMontant (int newVar) {
		montant = newVar;
	}

	/**
	 * Get the value of montant
	 * @return the value of montant
	 */
	public int getMontant () {
		return montant;
	}

}
