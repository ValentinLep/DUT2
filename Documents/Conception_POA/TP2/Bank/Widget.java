
import java.util.*;

public class Widget extends JFrame implements IObservateur {

	//
	// Fields
	//

	private JLabel m_message;	
	public Widget (String titre) {
		super(titre);
		this.setSize(300,200);
		this.message = new JLabel("Compte OK!");
		this.add(this.message);
		this.setVisible(true);
	};


	//
	// Accessor methods
	//

	/**
	 * Set the value of m_message
	 * @param newVar the new value of m_message
	 */
	private void setMessage (JLabel newVar) {
		m_message = newVar;
	}

	/**
	 * Get the value of m_message
	 * @return the value of m_message
	 */
	private JLabel getMessage () {
		return m_message;
	}

	/**
	 */
	public void notification(String info) {
		this.m_message.setText(info);
	};


}
