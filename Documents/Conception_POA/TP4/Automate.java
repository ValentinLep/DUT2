public class Automate {
    private Etat etatInitial;

    public Automate(Etat init) {
        this.etatInitial = init;
    }

    public void setEtatInit(Etat etat) {
        this.etatInitial = etat;
    }

    public boolean analyserMot(String mot) {return this.etatInitial.traiterMot(mot);}
}