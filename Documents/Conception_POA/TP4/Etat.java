import java.util.*;

public class Etat {
    protected List<Transition> transitions;

    public Etat() {
        this.transitions = new ArrayList<>();
    }

    public void addEtat(Etat etat, char symbole) {
        this.transitions.add(new Transition(etat, symbole));
    }

    public void remove(Transition transition) {
        this.transitions.remove(transition);
    }

    public boolean traiterMot(String mot) {
        if (mot.length() == 0) return false;
        for (Transition t : this.transitions) {
            if (mot.charAt(0) == (t.getSymbole())) {
                if (t.getArrivee().traiterMot(mot.substring(1))) return true;
            }
        }
        return false;
    }
}