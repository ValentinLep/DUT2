import java.util.*;

public class EtatFinal extends Etat {

    public EtatFinal() {
        super();
    }

    @Override
    public boolean traiterMot(String mot) {
        if (mot.length() == 0) return true;
        for (Transition t : this.transitions) {
            if (mot.charAt(0) == (t.getSymbole())) {
                if (t.getArrivee().traiterMot(mot.substring(1))) return true;
            }
        }
        return false;
    }
}