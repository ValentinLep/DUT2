import java.util.*;

public class MyAutomate {
    public static void main(String[] args) {
        // creation de l'automate

        /* Etat etat1 = new Etat();
        Etat etat2 = new Etat();
        Etat etat3 = new Etat();
        EtatFinal etat4 = new EtatFinal();

        etat1.addEtat(etat2, 'a');
        etat2.addEtat(etat2, 'a');
        etat2.addEtat(etat3, 'b');
        etat3.addEtat(etat3, 'b');
        etat3.addEtat(etat4, 'b');

        Automate monAutomate = new Automate(etat1);*/

        Etat etat1 = new Etat();
        Etat etat2 = new Etat();
        Etat etat3 = new Etat();
        Etat etat4 = new Etat();
        EtatFinal etat5 = new EtatFinal();

        etat1.addEtat(etat1, 'a');
        etat1.addEtat(etat1, 'b');
        etat1.addEtat(etat2, 'b');

        etat2.addEtat(etat1, 'b');
        etat2.addEtat(etat3, 'a');

        etat3.addEtat(etat3, 'b');
        etat3.addEtat(etat3, 'a');
        etat3.addEtat(etat4, 'b');

        etat4.addEtat(etat3, 'b');
        etat4.addEtat(etat5, 'a');

        etat5.addEtat(etat5, 'b');
        etat5.addEtat(etat5, 'a');

        Automate monAutomate = new Automate(etat1);

        // Saisie utilisateur
        Scanner saisieUtilisateur = new Scanner(System.in);
        boolean continuer = true;

        while (continuer) {
            System.out.println("Veuillez saisir un mot (q pour quitter)");
            String mot = saisieUtilisateur.next();

            if (mot.equals("q")) continuer = false;
            else {
                if (monAutomate.analyserMot(mot)) System.out.println("Le mot a été reconnu");
                else System.out.println("Le mot n'a pas été reconnu");
            }
        }
    }
}