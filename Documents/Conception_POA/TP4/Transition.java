public class Transition {
    private Etat arrivee;
    private char symbole;

    public Transition(Etat arrivee, char symbole) {
        this.symbole = symbole;
        this.arrivee = arrivee;
    }

    public char getSymbole() {return this.symbole;}
    public Etat getArrivee() {return this.arrivee;}
}