
import java.util.*;

public class Automate implements IEvent{

	public IEtat m_etatCourant;	
	public X controle;
	
	public Automate(X controle) {
		this.m_etatCourant = new Etat1();
		this.controle = controle;
	};
	


	/**
	 * Get the value of m_etatCourant
	 * @return the value of m_etatCourant
	 */
	public IEtat getEtatCourant () {
		return m_etatCourant;
	}

	public X getControle(){
		return this.controle;
	}

	public void changerEtatCourant(IEtat etat)
	{
		this.m_etatCourant = etat;
	}

	@Override
	public void evenement1(){
		this.m_etatCourant.evenement1(this);
	}

	@Override
	public void evenement2(){
		this.m_etatCourant.evenement2(this);
	}

}
