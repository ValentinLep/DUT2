
public class Etat1 implements IEtat {
	public Etat1 () { };

	@Override
	public void evenement1(Automate automate){
		automate.getControle().action1();
		automate.changerEtatCourant(new Etat2());
	}

	@Override
	public void evenement2(Automate automate){}


}
