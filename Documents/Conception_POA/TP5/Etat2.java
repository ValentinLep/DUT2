
public class Etat2 implements IEtat {
	public Etat2 () { };

	@Override
	public void evenement2(Automate automate){
		automate.getControle().action2();
		automate.changerEtatCourant(new Etat1());
	}

	@Override
	public void evenement1(Automate automate){}
}
