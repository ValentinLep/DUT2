import java.util.*;

public class MyX {
    public static void main(String[] args) {
        X monX = new X();

        Scanner saisieUtilisateur = new Scanner(System.in);
        boolean continuer = true;
        while(continuer){
            System.out.println("Choisir un event : \n\t1 -> événement 1\n\t2 -> événement 2\n\t0 -> quitter");
            String mot = saisieUtilisateur.next();

            switch(mot) {
                case "0":
                    continuer = false;
                    break;
                case "1":
                    monX.evenement1();
                    break;
                case "2":
                    monX.evenement2();
                    break;
            }
        }
        System.out.println("Bye");
    }
}