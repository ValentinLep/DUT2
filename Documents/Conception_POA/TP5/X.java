
import java.util.*;

public class X implements IEvent {


	public Automate m_controleur;	
	public X (){
		this.m_controleur = new Automate(this);
	};

	public void setControleur (Automate newVar) {
		m_controleur = newVar;
	}

	public Automate getControleur () {
		return m_controleur;
	}

	/**
	 */
	public void action1()
	{
		System.out.println("Je passe dans l'Etat2");
	}


	/**
	 */
	public void action2()
	{
		System.out.println("Je passe dans l'Etat1");
	}


	/**
	 */
	@Override
	public void evenement1(){
		this.m_controleur.evenement1();
	}


	/**
	 */
	@Override
	public void evenement2(){
		this.m_controleur.evenement2();
	}


}
