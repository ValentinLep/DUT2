#!/usr/bin/python3
"""
Module implementant la classe Convertisseur et la fonction convertir()
"""
#import sys

class NombrePasDansBonFormatException(Exception):
    """ Exception disant que le nombre d'entrée n'est pas écrit dans le bon format CONNARD"""

class Convertisseur: # pylint: disable=too-few-public-methods,too-many-statements,too-many-return-statements,too-many-branches
    """
    Classe qui permet de convertir un nombre d'un certain format en un autre.
    """
    def __init__(self, format_source, format_arrivee):
        """
        Constructeur qui prend 2 parametres:
            - str : format_source -> le format voulut de départ
            - str : format_arrivee -> le format voulut en arrivee
        """
        self.source = format_source
        self.arrivee = format_arrivee

    def verif_format_source(self):
        """ Methode qui verifie si le format_source de ma classe est un format pris en charge
        renvoie un bool : True si le format source est bon, False si non"""
        return self.source in {"decimal", "binaire"}

    def get_nombre(self, chaine):
        """
        Methode qui transforme une chaine en nombre
        parametre :
            - str : chaine -> la chaine de caractere de depart
        renvoie :
            - int : nombre de sorti
            - bool : est ce que le nombre est negatif
        """
        def get_info_format():
            """renvoie la trad, le multiplicateur selon le format d'entrée"""
            if self.source == "decimal":
                base = 10
            else:
                base = 2
            return {str(x):x for x in range(base)}, base
        nombre = 0
        negatif = False
        if chaine[0] == "-":
            chaine = chaine[1:]
            negatif = True
        trad, multiplicateur = get_info_format()
        for lettre in chaine:
            if lettre not in trad:
                raise NombrePasDansBonFormatException("ERREUR")
            nombre = nombre*multiplicateur+trad[lettre]
        return nombre, negatif

    def get_chaine(self, nombre):
        base = {"binaire":2, "hexa": 16, "octal": 8, "decimal": 10}[self.arrivee]
        chiffres = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C","D", "E", "F"]
        res = ""
        if nombre == 0:
            return "0"
        while nombre:
            res = chiffres[nombre%base] + res
            nombre = nombre//base
        return res

    def convertir(self, chaine):
        """
        Methode qui convertit une chaine de caractere prise en parametre selon
            les formats du convertisseur.
        """
        if not self.verif_format_source():
            return "Format d'entre mal gere"
        try:
            nombre, negatif = self.get_nombre(chaine)
        except NombrePasDansBonFormatException:
            return "ERREUR"
        res = ""

        if self.arrivee == "binaire":
            if nombre == 0:
                return "0"
            while nombre:
                if nombre%2 == 0:
                    res = "0"+res
                    nombre = nombre//2
                else:
                    res = "1"+res
                    nombre = nombre//2
            res = "-"+res if negatif else res
            return res
        if self.arrivee == "hexa":
            if nombre == 0:
                return "0"
            while nombre:
                res = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",\
                "A", "B", "C", "D", "E", "F"][nombre%16]+res
                nombre = nombre//16
            res = "-"+res if negatif else res
            return res
        if self.arrivee == "octal":
            if nombre == 0:
                return "0"
            while nombre:
                res = ["0", "1", "2", "3", "4", "5", "6", "7"][nombre%8]+res
                nombre = nombre//8
            res = "-"+res if negatif else res
            return res
        if self.arrivee == "decimal":
            if nombre == 0:
                return "0"
            while nombre:
                res = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"][nombre%10]+res
                nombre = nombre//10
            res = "-"+res if negatif else res
            return res
        if self.arrivee == "hexa":
            if nombre == 0:
                return "0"
            while nombre:
                res = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D",\
                "E", "F"][nombre%16]+res
                nombre = nombre//16
            res = "-"+res if negatif else res
            return res
        if self.arrivee == "octal":
            if nombre == 0:
                return "0"
            while nombre:
                res = ["0", "1", "2", "3", "4", "5", "6", "7"][nombre%8]+res
                nombre = nombre//8
            res = "-"+res if negatif else res
            return res
        return "Format d'entre mal gere"


def convertir(source, destination, chaine):
    """ Fonction qui convertit un format de nombre en un autre
    parametres:
        - source (str) : "decimal" or "binaire"
        - destination (str) : "hexa" ou "octal" ou "decimal"
        - chaine (str) : nombre à convertir
    return:
        string : le nombre convertit
    """
    conv = Convertisseur(source, destination)
    res = conv.convertir(chaine)
    if res != "ERREUR":
        return res
    return "ERREUR DE CONVERSION"
