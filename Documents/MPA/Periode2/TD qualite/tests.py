#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tests de l'application de conversion des nombres. 
Utiliser coverage pour le test de couverture :

 coverage 

"""
 

import unittest
from convertisseur import convertir

class TestConvertisseur(unittest.TestCase):
    def test_zero(self):
        self.assertEqual("0", convertir("decimal","binaire","0"))
        self.assertEqual("0", convertir("decimal","hexa","0"))
        self.assertEqual("0", convertir("decimal","octal","0"))
        self.assertEqual("0", convertir("binaire","octal","0"))
        self.assertEqual("0", convertir("binaire","decimal","0"))
        self.assertEqual("0", convertir("binaire","hexa","0"))
    
    def test_dix(self):
        self.assertEqual("1010", convertir("decimal","binaire","10"))
        self.assertEqual("A", convertir("decimal","hexa","10"))
        self.assertEqual("12", convertir("decimal","octal","10"))
        self.assertEqual("10", convertir("binaire","decimal","1010"))
        self.assertEqual("A", convertir("binaire","hexa","1010"))
        self.assertEqual("12", convertir("binaire","octal","1010"))
    def test_moins_dix(self):
        self.assertEqual("-1010", convertir("decimal","binaire","-10"))
        self.assertEqual("-A", convertir("decimal","hexa","-10"))
        self.assertEqual("-12", convertir("decimal","octal","-10"))
        self.assertEqual("-10", convertir("binaire","decimal","-1010"))
        self.assertEqual("-A", convertir("binaire","hexa","-1010"))
        self.assertEqual("-12", convertir("binaire","octal","-1010"))
    
    def test_ERREUR(self):
        self.assertEqual("ERREUR DE CONVERSION", convertir("decimal","binaire","A"))
        self.assertEqual("ERREUR DE CONVERSION", convertir("binaire","hexa","A"))
        self.assertEqual("Format d'entre mal gere", convertir("bébéboa","octal","10"))
        self.assertEqual("Format d'entre mal gere", convertir("binaire","bébéboa","10"))


if __name__ == '__main__':
    unittest.main()
