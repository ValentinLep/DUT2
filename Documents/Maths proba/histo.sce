function res = loiBinomiale(n,p)
        res = zeros(1,n+1);
    for k = 0:n
        Ckn = prod(n-k+1:n) /prod(1:k);
        res(k+1) = Ckn * p^k * (1-p)^(n-k);
    end
endfunction

clf;

F = 0:N;

subplot(2,2,1);
plot2d3(F, loiBinomiale(N,p), 5);


function loi1=loiPoisson(n,a)
    loi1 = zeros(1,n+1);
    for n1 = 0:n
        loi1(n1+1) = exp(-a)*a^n1/prod(1:n1);
    end
endfunction

N=100
para = 0.7;
lp = loiPoisson(N,para);
F = 0:N;

xtitle( "Densité loi Poisson  P(" + string(para)+") : référence") ;
subplot(2,2,1);
plot2d3(F,lp,4);



function G = Normale(m,s,p)
       x = m-p:0.1:m+p;
       G = exp(-((x-m).*(x-m))/(2*s^2))/sqrt(2*(%pi)*s^2);
endfunction


m=200*0.4;
sq=sqrt(N*p*(1-p));
pr=100
x = m-pr:0.1:m+pr
ln=Normale(m,sq,pr)
subplot(2,2,2)
xtitle( "Densité loi Normale  N(" + .. 
        string(m) + " , " + string(sq)+") : référence") ;
plot2d(x,ln,2);




function S = simBinomiale(n,p)
    S = 0;
    for k = 1:n
        U = rand();
        if  ( U < p )
            S = S + 1;
        end
    end
endfunction

N = 100;
p = 0.01;

K = 10000;
s = -0.5:N+0.5;
data = zeros(1,K);
for k = 1:K
    data(k) = simBinomiale(N,p);
end


subplot(2,2,2)
xtitle( "Binomiale, générée par un échantillon") ;
histplot(s, data);


function loi1=loiPoisson(n,a)
    loi1 = zeros(1,n+1);
    for n1 = 0:n
        loi1(n1+1) = exp(-a)*a^n1/prod(1:n1);
    end
endfunction


para = p*N;
lp = loiPoisson(N,para);
F = 0:N;

xtitle( "Densité loi Poisson  P(" + string(para)+") : référence") ;
subplot(2,2,2);
plot2d(F,lp,5);




