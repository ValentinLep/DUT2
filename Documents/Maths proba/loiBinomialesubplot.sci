// bino1.sci
//clear;

function loi = loiBinomiale(n,p)
    loi = zeros(1,n+1);
    for n1 = 0:n
        Combi = prod(n-n1+1:n) /prod(1:n1);
        loi(n1+1) = Combi * p^n1 * (1-p)^(n-n1);
    end
endfunction

function loi1 = loiPoisson(n,a)
    loi1 = zeros(1,n+1);
    for n1 = 0:n
        loi1(n1+1) = exp(-a)*a^n1/prod(1:n1);
    end
endfunction


function G = Normale(m,s,p)
       x = m-p:0.1:m+p;
       G = exp(-((x-m).*(x-m))/(2*s^2))/sqrt(2*(%pi)*s^2);
endfunction

function S = simBinomiale(n,p)
    S = 0;
    for k = 1:n
        U = rand();
        if  ( U < p )
            S = S + 1;
        end
    end
endfunction




// parametres de la loi binomiale
N = 100;
p = 0.4;


// initiation graphique
clf;


F = 0:N;
lb = loiBinomiale(N,p);
xtitle( "Densité loi binomiale B(" + .. 
        string(N) + " , " + string(p)+") : référence") ;

subplot(2,2,1)
plot2d3(F, lb, 5);


para=0.7;
lp = loiPoisson(N,para)

subplot(2,2,4)
xtitle( "Densité loi Poisson  P(" + string(para)+") : référence") ;
plot2d3(F,lp,4);


m=0;
sq=1;
pr=4
x = m-pr:0.1:m+pr
ln=Normale(m,sq,pr)
subplot(2,2,3)
xtitle( "Densité loi Normale  N(" + .. 
        string(m) + " , " + string(sq)+") : référence") ;
plot2d(x,ln,2);


// subdivision
s = -0.5:N+0.5;

// K tirages
K = 1000;
data = zeros(1,K);
for k = 1:K
    data(k) = simBinomiale(N,p);
end


subplot(2,2,2)
xtitle( "Binomiale, générée par un échantillon") ;
histplot(s, data);

//m=N*p;
//sq=sqrt(N*p*(1-p));
//pr=4*sq
//x = m-pr:0.1:m+pr
//ln=Normale(m,sq,pr)
//plot2d(x,ln,2);
