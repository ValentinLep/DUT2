function G = Normale(m,s,p)
       x = m-p:0.1:m+p;
       G = exp(-((x-m).*(x-m))/(2*s^2))/sqrt(2*(%pi)*s^2);
endfunction


m=4;
sq=3;
pr=7
x = m-pr:0.1:m+pr
ln=Normale(m,sq,pr)
subplot(2,2,3)
xtitle( "Densité loi Normale  N(" + .. 
        string(m) + " , " + string(sq)+") : référence") ;
plot2d(x,ln,2);
