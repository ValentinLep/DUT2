#ifndef exec
#define exec



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int nbMois = 12;
int gi = 5;
int Tab[] = { 1, 2, 3};
char Titre [50];

int main() {

    int i = 2;
    int* pti = &i; // pointeur sur i
    char* mes = "Il fait beau";
    char* ad;
    int T [2]; // tableau de 2 entiers
    char c;
    static int s = 10;
    ad = (char*) malloc (512); // allocation de 512 octets
    strcpy (Titre, "*******************************");

    printf("&main=%p\n", &main);
    printf("&nbMois=%p\tnbMois=%d\n", &nbMois, nbMois);
    printf("&gi=%p\tgi=%d\n", &gi, gi);
    printf("Tab=%p\t&Tab[0]=%p\tTab[0]-%d\n", Tab, &Tab [0], Tab [0]);
    printf("Titre=%p\t&Titre [0]=%p\tTitre [0]=%c\n", Titre, &Titre [0], Titre [0]);
    printf("\n&i=%p\ti=%d\t&pti=%p\tpti=%p\t pti=%d\n", &i, i, &pti, pti, *pti);
    printf("&mes=%p\tmes=%p\tmes [0]=%c\t*mes=%c\n", &mes, mes, mes [0], *mes);
    printf("&ad=%p\tad=%p\tad[0]=%c\t*ad=%c\n", &ad, ad, ad [0], *ad);
    printf("T=%p\t&T[0]=%p\tT[0]=%d\n", T, &T[0], T[0]);
    printf("&c=%p\tc=%c\n", &c, c);
    printf("&s=%p\ts=%d\n", &s, s);

    mes [0] = '*';

    printf("En attente... Observer l'organisation memoire_du_processus.cf. feuille de TD.");
    scanf("%c", &c); // bloque le processus pour examen
    return(0);

}
#endif