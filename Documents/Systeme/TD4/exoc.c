#include<pthread.h>
#include<stdio.h>
#include<unistd.h>

void  *fct (){
    sleep(20);
    printf("20s%d", 2);
}
void  *fct2 (){
    sleep(30);
    printf("30s%d", 2);
}
int main (){
    pthread_t t1;
    pthread_t t2;
    pthread_create(&t1, NULL, fct, NULL);
    pthread_create(&t2, NULL, fct2, NULL);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    return 0;
}
