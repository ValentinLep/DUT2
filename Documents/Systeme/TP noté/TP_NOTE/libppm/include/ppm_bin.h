#ifndef PPM_BIN_H
#define PPM_BIN_H


#include <ppm_struct.h>


ppm_t ppm_read_bin( char const * filename );
void ppm_write_bin( ppm_t const * pb, char const * filename );
void ppm_display_bin( ppm_t const * pb );


#endif