#ifndef PPM_STRUCT_H
#define PPM_STRUCT_H


typedef struct
{
  unsigned int w;
  unsigned int h;
  unsigned int max;
  unsigned char * data;
} ppm_t;


#endif