#include <ppm_ascii.h>
#include <IL/il.h>

ppm_t ppm_read_ascii(char const* filename)
{
  ppm_t tmp;

  FILE *file = fopen(filename, "r");

  if (file)
  {
    char magic[3] = "  \0";

    fscanf(file, "%2s", magic);

    if (strcmp(magic, "P3") == 0)
    {
      puts("PPM ASCII file");
    }
    else
    {
      exit(2);
    }

    fscanf(file, "%zu", tmp.w);
    fscanf(file, "%zu", tmp.h);
    fscanf(file, "%zu", tmp.max);

    unsigned char * hakunaMaDATA = (unsigned char *) malloc(tmp->w * tmp->h * 3);

    for (size_t i = 0; i < w * h; ++i)
    {
      fscanf(file, "%hu%hu%hu", hakunaMaDATA[i*3], hakunaMaDATA[i*3 + 1], hakunaMaDATA[i*3 + 2]);
    }

    tmp.data = hakunaMaDATA;

    fclose(file);
  }
  else
  {
    perror("error");
  }


  return tmp;
}

void ppm_write_ascii(ppm_t const* pa, char const* filename)
{
  FILE *file = fopen(filename, "w");

  if (file)
  {
    fprintf(file, "P3\n");

    fprintf(file, "%zu", tmp.w);
    fprintf(file, "%zu\n", tmp.h);
    fprintf(file, "%zu\n", tmp.max);

    unsigned char * hakunaMaDATA = (unsigned char *) malloc(tmp->w * tmp->h * 3);

    for (size_t i = 0; i < w * h; ++i)
    {
      fscanf(file, "%hu%hu%hu", hakunaMaDATA[i*3], hakunaMaDATA[i*3 + 1], hakunaMaDATA[i*3 + 2]);
    }

    tmp.data = hakunaMaDATA;

    fclose(file);
  }
  else
  {
    perror("error");
  }


  return tmp;
}


void ppm_display_ascii(ppm_t const* pa)
{

}