#include <ppm.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
  ppm_t in = ppm_read_ascii("project/images/in.ppm");
  ppm_display_ascii(&in);

  for (size_t i = 0; i < in.w * in.h * 3; ++i)
  {
    in.data[i] = 255 - in.data[i];
  }

  return 0;
}
