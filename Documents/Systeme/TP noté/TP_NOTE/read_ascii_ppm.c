#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
  if (argc == 1)
  {
    puts("error: missing filename.");
    exit(1);
  }

  FILE *file = fopen(argv[1], "r");

  if (file)
  {
    char magic[3] = "  \0";
    size_t w;
    size_t h;
    size_t max;

    unsigned short p[3];

    fscanf(file, "%2s", magic);

    if (strcmp(magic, "P3") == 0)
    {
      puts("PPM ASCI file");
    }
    else
    {
      exit(2);
    }

    fscanf(file, "%zu", &w);
    fscanf(file, "%zu", &h);
    fscanf(file, "%zu", &max);

    printf("%zu %zu\n%zu\n", w, h, max);

    for (size_t i = 0; i < w * h; ++i)
    {
      fscanf(file, "%hu%hu%hu", p, p + 1, p + 2);
      printf("%hu %hu %hu\n", p[0], p[1], p[2]);
    }

    fclose(file);
  }
  else
  {
    perror("error");
  }

  return 0;
}
