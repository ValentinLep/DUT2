Lecture, écriture et conversion de fichiers PPM
===============================================

L'objectif est de créer une bibliothèque permettant de lire, convertir des images au format PPM (Portable PixMap).

Le format PPM permet d'encoder des images en couleur soit deux formats :

* texte (ascii) : les pixels ont 3 composantes R, G, B avec des valeurs comprises entre 0 et MAX, par exemple `255 0 0` pour du rouge.
* binaire : les pixels ont des valeurs codées en hexadécimale entre 0 et MAX, par exemple `ff0000` pour un pixel rouge.

On se limitera dans notre bibliothèque à la gestion d'images dont la valeur MAX est 255.

Un fichier PPM commence par un en-tête au format texte :
1. un *magic id* indiquant si les données sont au format texte, `P3`, ou binaire, `P6`,
1. la taille de l'image est donnée au format `colonnes lignes`,
1. la valeur maximale des composantes, on supposera ici la valeur 255.

Les valeurs sont ensuite soit au format texte soit au format binaire. Attention au format texte, il ne faut pas dépasser 70 caractères par ligne, le plus simple est donc de mettre un pixel par ligne :

```
255 0 0
0 255 0
...
```

Le squelette de la bibliothèque se trouve dans le dossier `libppm`.
Pour tester si votre bibliothèque fonctionne, il faudra utiliser comme base le projet dans le dossier `project`. Vous devez également ajouter les Makefiles dans chaque projet.

# Exemple 

Pour tester la lecture de fichier PPM Binaire :
`make run`

# Lien

Plus d'infos sur le format PPM :
https://fr.wikipedia.org/wiki/Portable_pixmap

# TP noté

L'organisation de la lib est donnée, il reste à compléter le code pour lire et écrire les fichiers au format PPM et à faire les makefiles. **Il faut convertir une image ppm au format binaire à une image ppm au format ascii.**. Le projet "libppm" doit contenir les bibliothèques statiques et dynamiques dans le sous dossier lib et le projet "project" doit utiliser la lib pour s'exécuter (il faut que ça marche avec la lib statique et dynamique). J'ai mis un fichier read_bin_ppm.c pour montrer comment lire un fichier PPM au format ASCII, et un fichier read_bin_ppm.c pour montrer comment lire un fichier PPM au format Binaire.


