#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main( int argc, char * argv[] )
{
  if( argc == 1 )
  {
    puts( "error: missing filename." );
    exit( 1 );
  }
  
  FILE * file = fopen( argv[ 1 ], "r" );

  if( file )
  {
    char magic[ 3 ] = "  \0";
    size_t w;
    size_t h;
    size_t max;

    unsigned short p[ 3 ];
    
    fscanf( file, "%2s", magic );

    if( strcmp( magic, "P6" ) == 0 )
    {
      puts( "PPM binary file" );
    }
    else
    {
      exit( 2 );
    }

    fscanf( file, "%zu", &w );
    fscanf( file, "%zu", &h );
    fscanf( file, "%zu", &max );
    printf( "%zu %zu\n%zu\n", w, h, max );

    //allocate array to hold the pixels
    unsigned char *pixels = (unsigned char*)malloc(w*h*3*sizeof(unsigned char));
    fread(magic,1,1,file); //read newline character before the binary section
    fread(pixels,1,w*h*3,file); //read binary data
    for( size_t i = 0 ; i < w * h*3; i+=3 )
    {
      printf( "%u %u %u\n", pixels[i],pixels[i+1],pixels[i+2] );
    }
    
    fclose( file );
  }
  else
  {
    perror( "error" );
  }
  
  return 0;
}
