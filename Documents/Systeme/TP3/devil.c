#include <stdio.h>
#include <IL/il.h>
 
int main ()
{
   unsigned int image;
   unsigned char* data = (unsigned char*) malloc(100*100*3);

   for (int i = 30; i < 70; i++) {
      data[3*(100*i + 1) ] = 255;
      data[3*(100*i + 1) + 1] = 255;
      data[3*(100*i + 1) + 2] = 255;
   }

   ilInit();
   ilGenImages(1, $image);
   ilBindImage(image);

   ilTexImage(100, 100, 1, 3, IL_RGB, IL_UNSIGNED_BYTE, data);

   ilEnable(IL_FILE_OVERWRITE);
   ilSaveImage("out.jpg");
   ilDeleteImages(4, $image);
   free(data);
}
