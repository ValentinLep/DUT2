#include <stdio.h>
#include <IL/il.h>
#include <stdlib.h>
#include <math.h>
 
int main ()
{
   unsigned int image;
   unsigned char* data = (unsigned char*) malloc(100*100*3);

   for (int i = 30; i < 70; i++) {
      data[3*(100*i + 30) ] = 255;
      data[3*(100*i + 30) + 1] = 255;
      data[3*(100*i + 30) + 2] = 255;

      data[3*(100*i + 70) ] = 255;
      data[3*(100*i + 70) + 1] = 255;
      data[3*(100*i + 70) + 2] = 255;

      data[3*(100*30 + i) ] = 255;
      data[3*(100*30 + i) + 1] = 255;
      data[3*(100*30 + i) + 2] = 255;

      data[3*(100*70 + i) ] = 255;
      data[3*(100*70 + i) + 1] = 255;
      data[3*(100*70 + i) + 2] = 255;
   }


   float rayon = sqrt(2*40*40);



   ilInit();
   ilGenImages(1, &image);
   ilBindImage(image);

   ilTexImage(100, 100, 1, 3, IL_RGB, IL_UNSIGNED_BYTE, data);

   ilEnable(IL_FILE_OVERWRITE);
   ilSaveImage("out.jpg");
   ilDeleteImages(4, &image);
   free(data);
}
