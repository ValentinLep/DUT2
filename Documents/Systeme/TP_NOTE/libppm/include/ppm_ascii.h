#ifndef PPM_ASCII_H
#define PPM_ASCII_H


#include <ppm_struct.h>


ppm_t ppm_read_ascii( char const * filename );
void ppm_write_ascii( ppm_t const * pa, char const * filename );
void ppm_display_ascii( ppm_t const * pa );


#endif