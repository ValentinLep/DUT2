import numpy as np
import math
from matplotlib import pyplot

def f(x, r=1.2):
    return r*x

def eulerNulle(f, y0, t):
    y = [y0]
    for i in range(len(t)-1):
        y.append(y[-1]+(t[i+1]-t[i])*f(y[-1]))
    return y

y0 = 1
t1 = np.arange(0, 5, 0.1)
t1 = np.arange(0, 5, 0.001)










































def euler(f, z0, t):
    n = len(t)
    z = np.zeros((len(z0), n), float)
    z[:,0]= z0
    for i in range(1,n):
        z[:,i] = z[:,i - 1] + (t[i] - t[i-1]) * f(z[i-1])
    return z

def f3(z):
    x,y = z[0], z[1]
    return np.array((1.5*x-0.05*x*y, -0.43 * y + 0.05 * x*y))


t = np.arrange(0.50, 0.0005)
z0 = np.array((4,10))
z = euler(f3, z0, t)
pyplot(t, z[0, :])