

import numpy as np
import math
from matplotlib import pyplot as plt
from pylab import * 


k = 10
m = 1

def g(x, v):
    return -1 * (k/m)**2 * x


def masse_ressort_expl(g, x0, v0, t):
    N = len(t)
    X = [x0]
    V = [v0]
    for i in range(N - 1):
        h = t[i+1] - t[i]
        X.append(X[i] + h * V[i])
        V.append(V[i] + h * g(X[i], V[i])) 
    return X, V

#print(masse_ressort_expl(g, 1, 0, [t for t in range(0, 20, 0.001)]))
tmp = 0.001
tmp2 = int(1/tmp)
t = [temps * tmp for temps in range(0*tmp2, 20*tmp2, int(0.001*tmp2))]
x, v= masse_ressort_expl(g, 1, 0, t)
plt.subplot(2,1,1)
plt.plot(t, v, label="Vitesse")
plt.plot(t, x, label="Position")

plt.subplot(2,1,2)
plt.plot(x, v, label="Position")

legend()
plt.show()


def masse_ressort_semi_impl(g, x0, v0, t):
    N = len(t)
    X = [x0]
    V = [v0]
    for i in range(N - 1):
        h = t[i+1] - t[i]
        V.append(V[i] + h * g(X[i], V[i])) 
        X.append(X[i] + h * V[i+1])
    return X, V


tmp = 0.001
tmp2 = int(1/tmp)
t = [temps * tmp for temps in range(0*tmp2, 20*tmp2, int(0.001*tmp2))]
x, v= masse_ressort_semi_impl(g, 1, 0, t)
plt.subplot(2,1,1)
plt.plot(t, v, label="Vitesse")
plt.plot(t, x, label="Position")

plt.subplot(2,1,2)
plt.plot(x, v, label="Position")

legend()
plt.show()