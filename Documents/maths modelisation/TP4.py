import numpy as np
from random import *
from scipy.stats import *
from matplotlib import pyplot as plt
import math

"""N = [i + 1 for i in range(1000)]

moyEmp = [sum(bernoulli.rvs(0.1, size = n)) / n for n in N]

plt.plot(N, moyEmp, label="Moyenne empirique selon taille N")

plt.show()

vitConv = [math.sqrt(n) * ((sum(bernoulli.rvs(0.1, size = n)) / n) - 0.1) for n in N]

plt.plot(N, vitConv)

plt.show()"""


"""n = 4
collection = [i for i in range(n)]

def tirerCarte():
    global collection
    return choice(collection)

def avoirTout(maCollec):
    global collection
    return all(i in maCollec for i in collection)
        

def iterCollecDuo():
    maCollec = set()
    saCollec = set()
    j = 0
    while not avoirTout(maCollec) or not avoirTout(saCollec):
        j += 1
        maCarte = tirerCarte()
        saCarte = tirerCarte()
        if (maCarte not in maCollec): maCollec.add(maCarte)
        else: saCollec.add(maCarte)
        if (saCarte not in saCollec): saCollec.add(saCarte)
        else: maCollec.add(saCarte)
    if (not avoirTout(maCollec)):
        while not avoirTout(maCollec):
            j += 1
            maCollec.add(tirerCarte())
    elif (not avoirTout(saCollec)):
        while not avoirTout(saCollec):
            j += 1
            saCollec.add(tirerCarte())
    return j

def moyenneDuo(nombreIter):
    return(sum(iterCollecDuo() for _ in range(nombreIter)) / float(nombreIter))"""

nombreClients = 100
n = nombreClients
mu = 1

#tempsServiceParClient = expon.rvs(scale= 1/v, size=n)

def T():
    global mu, n
    tempsArrivees = expon.rvs(scale= 1/mu, size=n)
    return [sum(tempsArrivees[:k+1]) for k in range(n)]

monT = T()
#print(lesArrivees)
#plt.plot(range(n), lesArrivees, label="Moyenne empirique selon taille N")
#plt.show()

def N(tps):
    global T
    return sum([Ti < tps for Ti in T])

#print(N(40))

nu = 1

def R():
    global nu, n
    return list(expon.rvs(scale=1/nu, size=n))

monR = R()
print(monR)

def U():
    global monT, monR, n
    monU = [monT[0]]
    for k in range(1, n):
        # Uk = max(Uk−1 + Rk−1,Tk).
        monU.append(max(monT[k], monU[k-1] + monR[k - 1]))
    return monU

monU = U()
print(monU)

def S(tps):
    global U
    return sum([Ui < tps for Ui in U])