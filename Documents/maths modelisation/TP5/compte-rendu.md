# 1

## 1

La loi de bernoulli permet de simuler des experiences à 2 résultats en boucle. Ici ce sont des lancés de pièces ayant 2 résultats : pile ou face.

## 2

```python
from scipy.stats import bernoulli, norm

r = bernoulli.rvs(2/3, size=50)

print(r)
```
affichage : [0 1 1 0 1 0 1 0 0 0 1 0 0 1 1 0 1 0 0 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 0 0 0 1 1 0 1 1]
## 3

```python
N = len(r)
Xn = sum(r) / N
print(Xn)
```
affichage : 0.64
## 4

Quand n est très grand : la difference entre la moyenne empirique et l'esperence de X est minime.

## 5

```python
alpha = 0.05

Ua = norm.ppf(1-alpha/2)

avant = Xn - sqrt(Xn * (1-Xn)/N) * Ua
apres = Xn + sqrt(Xn * (1-Xn)/N) * Ua

print(avant, apres, sep="< Xn <")
```
affichage : 0.506953232862431< Xn <0.773046767137569

## 6

Pour 2% : 0.48208226971521406< Xn <0.797917730284786
Pour 1% : 0.46514690869902886< Xn <0.8148530913009712