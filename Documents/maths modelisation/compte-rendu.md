# Question 1

## 1.

376 accidents / 10 ans = 37.6 accidents / ans
37.6 accidents / ans / 365 mois = 0.1030.. accidents / jours

## 2.

```python
from numpy import *
from random import *
from scipy.stats import *

tab = bernoulli.rvs(0.1, size = 365)
print(tab) # tableau de 365 0 ou 1 selon la loi de Bernoulli avec p = 0.1 et N = 365
```

## 3.

```python
print(sum(tab[:22])) # j'ai trouvé 2
```

Ce nombre correspond au nombre de jours avec accidents qu'il y a eu lieu en 22 jours

## 4.

```python
tab = bernoulli.rvs(0.1, size = 365)

max = None
for i in range(365 - 21):
    somme = sum(tab[i:i+22])
    if max is None or max < somme:
        max = somme

print(tab)
print(max) # resultat souvent compris entre 5 et 7 inclus
```

Ce maximum correspond à : pour chaque periode de 22 jours durant l'année que représente tab, le maximum de journée avec accidents.

## 5.

D'apres les resultats précédent : avoir au moins 5 accidents en 22 jours est en fait plutot courant.

```python
def unEchantillon():
    tab = bernoulli.rvs(0.1, size = 365)

    max = None
    for i in range(365 - 21):
        somme = sum(tab[i:i+22])
        if max is None or max < somme:
            max = somme
    return max

echantillonGrandeTaille = [unEchantillon() >= 5 for _ in range(10000)]
print(sum(echantillonGrandeTaille) / len(echantillonGrandeTaille))
```

Je trouve environ que p = 0.92. La probabilité que dans une année, il y a plus de 5 jours avec accidents sur une periode de 22 jours est donc plutot élevé, c'est en fait une stat courante.

## 6

```python
N = [i + 1 for i in range(1000)]

moyEmp = [sum(bernoulli.rvs(0.1, size = n)) / n for n in N]

plt.plot(N, moyEmp, label="Moyenne empirique selon taille N")

plt.show()
```

![image info](./moyEmp.png)

Nous spouvons constater que la moyenne empirique converge vers 0.1 ce qui équivaut au paramètre p de la loi de Bernoulli (et de son espérence).

```python
vitConv = [math.sqrt(n) * ((sum(bernoulli.rvs(0.1, size = n)) / n) - 0.1) for n in N]

plt.plot(N, vitConv)

plt.show()
```

![image info](./vitConv.png)

Ici, la vitesse de convergence ne semble pas dépendre de N car la courbe ne ratréci ou ne grandit pas en largeur. 

# Question 2

## 1

P(t1) = 1 -> probabilité moyenne d'avoir 1 nouvelle vignette à l'instant t1
P(t2) = N-1/N -> probabilité moyenne d'avoir 1 nouvelle vignette à l'instant t2
Quand N est grand : Nlog(N)

Si il y a n vignettes, en moyenne il devrait acheter Nlog(N) paquets pour tous les avoir.

```python
# à partir d'ici : l'import de numpy devient : import numpy as np

n = 4
collection = [i for i in range(n)]

def tirerCarte():
    global collection
    return choice(collection)

def avoirTout(maCollec):
    global collection
    return all(i in maCollec for i in collection)

def iterCollec():
    maCollec = set()
    j = 0
    while not avoirTout(maCollec):
        j += 1
        maCollec.add(tirerCarte())
    return j

def moyenne(nombreIter):
    return(sum(iterCollec() for _ in range(nombreIter)) / float(nombreIter))

print(moyenne(1000))
```

Pour n = 4 : je trouve environ 8.2
Pour n = 20 : je trouve environ 71
Pour n = 100 : je trouve environ 520

## 2

```python
def nbDif(maCollec):
    colCourante = []
    for carte in maCollec:
        if carte not in colCourante:
            colCourante.append(carte)
    return len(colCourante)
        

def iterCollecDif():
    global n
    maCollec = set()
    j = 0
    nbDifPrecedent = 0
    tabDif = {}
    while nbDif(maCollec) != n:
        j += 1
        maCollec.add(tirerCarte())
        if nbDif(maCollec) != nbDifPrecedent:
            nbDifPrecedent = nbDif(maCollec)
            tabDif[nbDifPrecedent] = j
    return tabDif

def toutesIterDif(nombreIter):
    global n
    tabTotDif = {i + 1 : [] for i in range(n)}
    for _ in range(nombreIter):
        tabDifCourant = iterCollecDif()
        for k in tabTotDif.keys():
            tabTotDif[k].append(tabDifCourant[k])
    return tabTotDif

def moyenneDif(nombreIter):
    global n
    tabTotDif = toutesIterDif(nombreIter)
    tabMoyDif = {}
    for k, v in tabTotDif.items():
        tabMoyDif[k] = sum(v) / float(nombreIter)
    return tabMoyDif

print(moyenneDif(1000))
```

Ce code permet de calculer pour 1000 ouvertures la moyenne, pour chaque nombre de carte differentes, d'esperance du nombre d'ouvertures à faire. Avec n = 20.

Reponse obtenue : {1: 1.0, 2: 2.048, 3: 3.17, 4: 4.354, 5: 5.605, 6: 6.904, 7: 8.316, 8: 9.892, 9: 11.595, 10: 13.459, 11: 15.504, 12: 17.729, 13: 20.327, 14: 23.19, 15: 26.504, 16: 30.655, 17: 35.426, 18: 41.973, 19: 51.885, 20: 72.086}

Il est normal que jena a l'impression que certaines vignettes sont plus rares car plus il a de cartes différentes et moins il a de chances d'en avoir une nouvelle.

## 3

```python
# on ajoute ici une deuxieme collection qui sera la collection de Zacharie et si l'un a déjà une carte, il l'a donne à l'autre
# Aussi si l'un fini sa collection, alors il arrete de tirer mais l'autre continue
def iterCollecDuo():
    maCollec = set()
    saCollec = set()
    j = 0
    while not avoirTout(maCollec) or not avoirTout(saCollec):
        j += 1
        maCarte = tirerCarte()
        saCarte = tirerCarte()
        if (maCarte not in maCollec): maCollec.add(maCarte)
        else: saCollec.add(maCarte)
        if (saCarte not in saCollec): saCollec.add(saCarte)
        else: maCollec.add(saCarte)
    if (not avoirTout(maCollec)):
        while not avoirTout(maCollec):
            j += 1
            maCollec.add(tirerCarte())
    elif (not avoirTout(saCollec)):
        while not avoirTout(saCollec):
            j += 1
            saCollec.add(tirerCarte())
    return j

def moyenneDuo(nombreIter):
    return(sum(iterCollec() for _ in range(nombreIter)) / float(nombreIter))

print(moyenne(1000))
```

Pour n = 4 : je trouve environ 7.2 au lieu de 8.2, on gagne donc 1 tirage
Pour n = 20 : je trouve environ 54.75 au lieu de 71 on gagne donc 16.25 tirages
Pour n = 100 : je trouve environ 360 au lieu de 520 on gagne donc 160 tirages

On remarque d'après ces résultats que mettre en commun leurs vignettes est rentable. Aussi plus le nombre de cartes à colelctionner est grand, plus le nombre de tirages gagnés est grand

# Question 3

## 1
```python
nombreClients = 100
n = nombreClients
mu = 1

def T():
    global mu, n
    tempsArrivees = expon.rvs(scale= 1/mu, size=n)
    return [sum(tempsArrivees[:k+1]) for k in range(n)]

# monT est une instance de T()
monT = T()
# Voici les temps d'arrivées de chaques client pour mu = 1 et 100 clients
print(monT)
# Et un petit graphique pour mieux visualiser
plt.plot(range(n), monT, label="Moyenne empirique selon taille N")
plt.show()
```
Au 100eme client, on a bien une moyenne de temps d'arrivée à 100 pour mu = 1.

## 2
```python
def N(tps):
    global T
    # N(t) = la somme de tout les temps d'arrivées inférieurs au temps en parametre
    return sum([Ti < tps for Ti in T])
```
## 3
```python
nu = 1

def R():
    global nu, n
    # expon.rvs() renvoie un numpy.ndarray et affiche les nombre en écriture scientifique. Je le remet en list() pour mieux observer par la suite
    return list(expon.rvs(scale= 1/nu, size=n))

monR = R()
print(monR)
```
## 4
U0 = T0 -> au départ, le premier client arrive au guichet au moment où il arrive dans la file étant donné qu'il y est seul.

∀k ≥1, Uk = max(Uk−1 + Rk−1,Tk) -> Par la suite, soit quand le client arrive, la file est vide et dans ce cas là c'est comme pour U0. Mais dans le cas où la file a d'autre personnes : le client arrive au guichet quand le précédent client a fini avec le guichet donc son temps d'arrivé + temps qu'il a pris au guichet. Enfin on prend le max des 2 car les 2 conditions doivent être réalisées.
## 5

## 6
## 7
## 8
## 9
## 10
## 11
## 12
## 13

