#include <stdio.h>
#include <stdlib.h>

int main() // ne jamais oublier le return 0; pour la sortie d'erreur
{
    puts("Hello World");

    // TYPES

    // 8 bits  16b      32b    64b
    // char -> short -> int -> long long (int)
    // unsigned char -> unsigned short -> unsign....

    // float (32b) -> double (64b)

    /* printf( "%d/%i(int)  
                %c(char)  
                %lli(long long int)  
                %u(unsigned int)  
                %hhi(half half i = char)  
                %hhu(half half unsigned int = unsigned char))
                %f(float)
                %lf(long float = double)
                ", entier, entier, caractere, long_long_entier, entier_non_signé, demi_demi_entier, demi_d....)
    */

    // TABLEAUX STATIQUES

    int tab[5]; // tableau d'entier pouvant en contenir 5

    int tab2[] = {1, 2, 3} // tableau de 3 entiers

    // FONCTIONS

    void hello() {puts("Hello");}
    hello();

    // POINTEURS (int* pa, &a, *pa)

    int a = 5;
    int* pa = &a; // pa = adresse vers a
    printf("%i\n", *pa); // affiche la valeur à l'adresse pa

    free(pa);

    // ALLOCATION DYNAMIQUE -> ATTENTION! TOUJOURS free() quand on en a plus besoin, sinon fuite memoire probable

    int* p = (int*)/* caste le void* renvoyé par malloc */ malloc(sizeof(int));
    *p = 3;

    free(p);  // libérer de la place se fait pas forcément direct :
    p = NULL; // le systeme met le free() sur liste d'attente donc il faut p = NULL; pour
              // ne pas réutiliser cette adresse qui sera libérer un de ces 4.


    size_t const size = 10; // permet de réutiliser la taille du tableau plus tard

    int* ptab = (int*) malloc(size * sizeof(int)); // ou malloc(10 * sizeof(int))
    *ptab = 3; // 1ere valeur du tableau
    ptab[0] = 6; // 1ere valeur du tableau, mode Java
    *(ptab + 5) = 1; // 6eme valeur du tableau
    ptab[6] = 2; // 6eme valeur du tableau, mode Java

    for( size_t i = 0; i < size ; ++i)
    {
        printf("%i\n", *ptab + i);
    }

    free(ptab);

    // STRUCTURE

    struct point2d // au niveau des fonctions
    {
        float x, y;
    }

    int main()
    {
        struct point2d p0 = {1.0f, 1.0f}; // le type de ma structure est "struct point2d"

        p0.x += 1.0f;

        return 0;
    }

    typedef struct
    {
        float x,y;
    } point2d_t; // ici le type de ma structure sera "point2d_t"
                 // convention : les struct avec typedef sont préfixés avec _t


    // AUTRE

    #define PI 3.14 // debut de programme
    #define double(a) a+a // remplacement la chaine PI par 3.14

    #ifdef PLUS5
    #else

    
    return 0; // OBLIGATOIRE LE RETOUR SUR SORTIE D'ERREUR
}