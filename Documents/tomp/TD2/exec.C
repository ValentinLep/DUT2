#ifndef exec
#define exec


#include <stdio.h>

const int cil = 5; // TEXT : constant

const char* cc1 = "Affichagel\n"; //TEXT et DATA
const char* cc2 = "Affichage2\n";

int gil = 3; // DATA
int gi2; // BSS

static int sgil; // BSS

int fct1() {

    static int si1 = 5; // DATA
    int i, j; // STACK

    i = si1 * 2;
    j = si1 - 1;

    si1 += 1;

    return i + j;
}

int fct2(int arg) {

    int i = 5;
    int j = i + fct1();

    return i + j;// + cil;
}

int main() {

    int a, b;

    printf("%s\n", cc1 );

    a = fct1();
    b = fct2 (a);

    printf("a-%d\tb=%d\n", a, b);
    printf("%s\n", cc2);

return 0;

}
#endif