<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste à puces</title>
</head>
<body>
    <h1>Liste des contacts</h1>
    <?php 
        try{
            $file_db=new PDO('sqlite:/tmp/contacts.sqlite3');
            $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

            echo "<ul>";
            $result=$file_db->query('SELECT * from contacts');
            foreach ($result as $m){
                echo "<li>".$m['prenom'].' '.$m['nom'].' '
                .date('Y-m-d H:i:s',$m['time'])."</li>";
            }
            echo "</ul>";
        }
        catch(PDOException $ex){
            echo $ex->getMessage();
        }
    ?>
</body>
</html>