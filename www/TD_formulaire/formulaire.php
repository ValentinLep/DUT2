<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="formulaire.css">
    <title>Formulaire</title>
</head>
<body>
    <header>
        <h1>Formulaire</h1>
        <h5>Veullez répondre au formulaire</h5>
    </header>

    <main>
        <?php

            class QuestionText
            {
                // déclaration d'une propriété
                public $name;
                public $question;
                public $reponses; // peut etre un str ou une liste
                public $score;
                public $id;

                public function __construct($q, $n, $r, $s, $id) {
                    $this->question = $q;
                    $this->name = $n;
                    $this->reponses = $r;
                    $this->score = $s;
                    $this->id = $id;
                    $this->value = $value;
                }

                public function afficheQuestion() {
                    echo "<li><label for=".$this->id.">".$this->question."</label><br/><input type='text' name='".$this->name."' id='".$this->id."' value='".$_POST[$this->name]."'></li>";
                }

                public function getQuestion() {return $this->question;}
                public function getName() {return $this->name;}
                public function getReponses() {return $this->reponses;}
                public function getScore() {return $this->score;}
                public function getId() {return $this->id;}
                public function getValue() {return $this->value;}

                public function reponse($rep) 
                {
                    if (gettype($this->reponses) == "string") return strcmp(strtolower($rep), strtolower($this->reponses)) == 0;
                    foreach ($this->reponses as $r) {
                        if (strcmp(strtolower($rep), strtolower($this->reponses)) == 0) return 1;
                    }
                    return 0;
                }
            }

            class QuestionRadio extends QuestionText
            {
                public $liste;

                public function __construct($q, $n, $r, $s, $id, $l) {
                    $this->liste = $l;
                    parent::__construct($q, $n, $r, $s, $id);
                }

                public function afficheQuestion() {
                    $aff = "<li>". $this->getQuestion() . "<br/>";
                    $i = 0;
                    foreach($this->liste as $l){
                        $i += 1;
                        $aff .= "<input type='radio' name='".$this->getName()."' value='$l' id='".$this->getId()."-$i'";
                        if ($_POST[$this->getName()] == $l) $aff .= "checked";
                        $aff .= ">";
                        $aff .= "<label for='".$this->getId()."-$i'>$l</label>";
                    }
                    echo $aff."</li>";
                }
            }

            class QuestionCheckbox extends QuestionRadio
            {
                public function afficheQuestion() {
                    $aff = "<li>". $this->getQuestion() . "<br/>";
                    $i = 0;
                    foreach($this->liste as $l){
                        $i += 1;
                        // ici on ajoute au name '[]' pour que la réponse envoyée dans le $_POST soit un array
                        $aff .= "<input type='checkbox' name='".$this->getName()."[]' value='$l' id='".$this->getId()."-$i'";
                        if (isset($_POST[$this->getName()])) if (in_array($l, $_POST[$this->getName()])) $aff .= "checked";
                        $aff .= ">";
                        $aff .= "<label for='".$this->getId()."-$i'>$l</label>";
                    }
                    echo $aff."</li>";
                }

                public function reponse($rep) 
                {
                    if (gettype($this->getReponses()) == "string") {
                        if (count($rep) == 1) {
                            return strcmp(strtolower($rep[0]), strtolower($this->reponses)) == 0;
                        }
                        return 0;
                    }

                    foreach ($rep as $r) {
                        if (!in_array($r, $this->getReponses())) {
                            return 0;
                        }
                    }

                    foreach ($this->getReponses() as $r) {
                        if (!in_array($r, $rep)) {
                            return 0;
                        }
                    }
                    
                    return 1;
                }
            }

            function affRep($tab) {
                // $tab est soit un string soit un tableau
                if (gettype($tab) == "string") return $tab;

                $res = "";
                foreach ($tab as $elem) {
                    $res.= $elem." ";
                }
                return $res;
            }
            
            $questions = [
                new QuestionText("Couleur de l'herbe", "herbe", "vert", 1, "id1"),
                new QuestionText("Couleur du sang", "sang", "rouge", 1, "id2"),
                new QuestionRadio("Couleur du ciel", "ciel", "Bleu", 1, "id3", ["Bleu", "Vert", "Jaune"]),
                new QuestionCheckbox("Couleur du soleil", "soleil", "Jaune", 1, "id4", ["Bleu", "Vert", "Jaune"]),
                new QuestionCheckbox("Couleur du metal et du feu", "metalFeu", ["Rouge", "Gris"], 2, "id5", ["Bleu", "Vert", "Jaune", "Rouge", "Gris"]),
                new QuestionRadio("1 - 1", "zero", "0", 1, "idZero", ["0", "1", "2"])
            ];


            if (! $_POST["repondu"]){
                // on recupere les infos de la session créée quand l'utilisateur n'a pas répondu à toutes les questions
                session_start();
                $_POST = $_SESSION;
                session_destroy();

                if (!empty($_POST)) echo "<h2>Veuillez répondre à toutes les questions</h2>\n";

                // On présente les questions
                echo "<form action='formulaire.php' method='post'><ul>\n";
                
                foreach ($questions as $q) {
                    $q->afficheQuestion();
                }

                // permet de dire que la page doit maintenant passer au côté réponses / score
                echo '<input type="hidden" name="repondu" value="1">';
    
                echo "</ul><button type='submit'>Repondre</button></form>";
            }
            else{
                // on verifie que l'utilisateur a bien répondu à toutes les questions
                // sinon on renvoie sur la meme page mais avec les infos sur les déjà champs remplis
                foreach ($questions as $q) {
                    $repDonnee = $_POST[$q->getName()];
                    if (null == $repDonnee){
                        session_start();
                        $_SESSION = $_POST;
                        $_SESSION["repondu"] = 0;
                        session_write_close();
                        header("Location: formulaire.php");
                    }
                }
                // On répond au client et on calcule son score
                $questions_total = 0;
                $questions_total=0;
                $question_correct=0;
                $score_max=0;
                $score=0;

                echo "<h1>Réponses à votre questionnaire</h1>";

                echo "<ul>";

                foreach ($questions as $q){
                    $questions_total += 1;
                    $score_max += $q->getScore();
                    $reponseDonnee = $_POST[$q->getName()];

                    $succes = $q->reponse($reponseDonnee);
                    

                    $score += $succes * $q->getScore();
                    $question_correct += $succes;
                    echo "<li>".$q->getQuestion()."&emsp;|&emsp;Score : ".$succes * $q->getScore()." / ".$q->getScore();
                    if (!$succes) {
                        echo "</br>Votre réponse : ".affRep($reponseDonnee)."</br>Reponse attendue : ".affRep($q->getReponses());
                    }
                    echo "</li>";
                }

                echo "</ul>";

                echo "Réponses correctes:" . $question_correct . "/" . $questions_total ."<br/>";
                echo "Votre score: " . $score . "/" . $score_max ."<br/>";
            }

            
        ?>

    </main>
</body>
</html>