<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Petit Quizz</title>
</head>
<body>
    <h1>Petit Quizz sur notre "petite" culture générale.</h1>
    <?php
        $questions = [
            array(
                "name" => "temperature",
                "type" => "radio",
                "text" => "A quelle température l'eau s'évapore ?",
                "choices" => [
                    array(
                        "text" => "10°C",
                        "value" => "dix"),
                    array(
                        "text" => "0°C",
                        "value" => "zero"),
                    array(
                        "text" => "100°C",
                        "value" => "cent"),
                    array(
                        "text" => "85°C",
                        "value" => "huit-cinq"),
                    ],
                "answer" => "cent",
                "score" => 1
            ),
            array(
                "name" => "macron",
                "type" => "text",
                "text" => "Quel est le nom de famille d'Emmanuel Macron ?",
                "answer" => "macron",
                "score" => 1
            ),
            array(
                "name" => "capitale",
                "type" => "checkbox",
                "text" => "Quelle est la capitale de la France ?",
                "choices" => [
                    array(
                        "text" => "Madrid",
                        "value" => "madrid"),
                    array(
                        "text" => "Paris",
                        "value" => "paris"),
                    array(
                        "text" => "Tokyo",
                        "value" => "tokyo"),
                    array(
                        "text" => "Rome",
                        "value" => "rome"),
                    ],
                "answer" => array("paris", "rome"),
                "score" => 1
            )
                ];

        $questions_total = 0;
        $question_correct = 0;
        $score_max = 0;
        $score = 0;

        // Affichage d'une question de type text
        function question_text($q){
            echo $q['text'] ."<br/><input type='text' name='$q[name]'><br/>";
        }
        // Affichage d'une question de type radio
        function question_radio($q){
            $html = $q['text'] . "<br/>";
            $i = 0;
            foreach($q['choices'] as $c){
                $i += 1;
                $html .= "<input type='radio' name='$q[name]' value='$c[value]' id='$q[name]-$i'>";
                $html .= "<label for='$q[name]-$i'>$c[text]</label>";
            }
            echo $html;
        }
        // Affichage d'une question de type checkbox
        function question_checkbox($q){
            $html = $q['text'] . "<br/>";
            $i = 0;
            foreach($q['choices'] as $c){
                $i += 1;
                $html .= "<input type='checkbox' name='$q[name][]' value='$c[value]' id='$q[name]-$i'>";
                $html .= "<label for='$q[name]-$i'>$c[text]</label>";
            }
            echo $html;
        }

        // traitement de la réponse à une question de type text
        function answer_text($q,$v){
            global $question_correct,$score_max, $score;
            $score_max += $q['score'];
            if (is_null($v)) return;
            if ($q['answer'] == $v){
                $question_correct += 1;
                $score += $q['score'];
            }
        }

        function answer_checkbox($q, $v) {
            global $question_correct,$score_max, $score;
            $score_max += $q['score'];
            echo $v[0];
            echo $v[1];
            echo $v[2];
            echo $v[3];
            $z = 0;
            echo $v[$z];
            if (is_null($v)) return;
            for ($i = 0; $i < count($v); $i++) {
                echo "<br/>";
                print_r($v);
                echo $i;
                echo $v[$i];
                print_r($q['answer']);
                if (!in_array($v[$i], $q['answer'])) {
                    echo "erreur<br/>";
                    return;
                }
            }
            echo "bien joué";
            $question_correct += 1;
            $score += $q['score'];
        }

        $question_handlers = array(
            "text" => "question_text",
            "radio" => "question_radio",
            "checkbox" => "question_checkbox"
        );
        $answer_handlers = array(
            "text" => "answer_text",
            "radio" => "answer_text",
            "checkbox" => "answer_checkbox"
        );

        if ($_SERVER['REQUEST_METHOD'] == 'GET'){
            // On présente les questions
            echo "<fieldset>
            <legend>Quizzz</legend><br/><br/>";
            echo "<form method='POST' action='quizz.php'><ol>";
            foreach ($questions as $q){
                echo "<li>";
                $question_handlers[$q['type']]($q);
            }
            echo "</ol><input type='submit' value='Repondre'></form><fieldset>";
        }
        else{
            // On répond au client et on calcule son score
            $questions_total = 0;
            $questions_total=0;
            $question_correct=0;
            $score_max=0;
            $score=0;
            foreach ($questions as $q){
                $questions_total += 1;
                $answer_handlers[$q['type']]($q, $_POST[$q['name']] ?? NULL);
            }
            echo "Réponses correctes:" . $question_correct . "/" . $questions_total ."<br/>";
            echo "Votre score: " . $score . "/" . $score_max ."<br/>"; 
        }
    ?>
</body>
</html>